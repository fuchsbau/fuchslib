/*
 * Copyright (c) 2019. All Rights Reserved.
 */

package de.fuchspfoten.fuchslib.util;

import org.bukkit.Bukkit;

import java.lang.reflect.Field;

/**
 * Provides utilities for reflection.
 */
public final class ReflectionHelper {

    /**
     * Returns the private field, made accessible.
     *
     * @param clazz  The class in which the field is located.
     * @param field  The field name.
     * @param object The object to read from.
     * @return The field, made accessible.
     */
    public static Object getPrivateField(final Class<?> clazz, final String field, final Object object) {
        try {
            final Field result = clazz.getDeclaredField(field);
            result.setAccessible(true);
            return result.get(object);
        } catch (final NoSuchFieldException | IllegalAccessException ex) {
            throw new IllegalArgumentException("failed to get field", ex);
        }
    }

    /**
     * Sets the private field, made accessible.
     *
     * @param clazz  The class in which the field is located.
     * @param field  The field name.
     * @param object The object to write to.
     * @param value  The value to write.
     */
    public static void setPrivateField(final Class<?> clazz, final String field, final Object object,
                                       final Object value) {
        try {
            final Field result = clazz.getDeclaredField(field);
            result.setAccessible(true);
            result.set(object, value);
        } catch (final NoSuchFieldException | IllegalAccessException ex) {
            throw new IllegalArgumentException("failed to set field", ex);
        }
    }

    /**
     * Obtains the class with the given name from the NMS package.
     *
     * @param name The name of the class.
     * @return The class.
     */
    public static Class<?> getNMSClass(final String name) {
        try {
            final String version = Bukkit.getServer().getClass().getPackage().getName().split("\\.")[3];
            final String fullName = String.format("net.minecraft.server.%1$s.%2$s", version, name);
            return Class.forName(fullName);
        } catch (final ClassNotFoundException ex) {
            throw new IllegalArgumentException("failed to find class", ex);
        }
    }

    /**
     * Obtains an entity ID.
     *
     * @return The obtained ID.
     */
    public static int getEntityId() {
        final Class<?> entityClass = getNMSClass("Entity");
        final int curValue = (Integer) getPrivateField(entityClass, "entityCount", null);
        setPrivateField(entityClass, "entityCount", null, curValue + 1);
        return curValue;
    }

    /**
     * Private constructor to prevent instance creation.
     */
    private ReflectionHelper() {
    }
}
