package de.fuchspfoten.fuchslib.util.primitive;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * A pair of integers with mutable values.
 */
@AllArgsConstructor
@ToString
@EqualsAndHashCode
public class ValueMutableIntPair {

    /**
     * The key.
     */
    private @Getter final int key;

    /**
     * The value.
     */
    private @Getter @Setter int value;
}
