/*
 * Copyright (c) 2019. All Rights Reserved.
 */

package de.fuchspfoten.fuchslib.util.primitive;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

/**
 * A pair of integers.
 */
@AllArgsConstructor
@ToString
@EqualsAndHashCode
public class IntPair {

    /**
     * The key.
     */
    private @Getter final int key;

    /**
     * The value.
     */
    private @Getter final int value;
}
