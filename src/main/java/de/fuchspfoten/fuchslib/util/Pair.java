package de.fuchspfoten.fuchslib.util;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

/**
 * A pair of two objects.
 *
 * @param <K> The type of the first object.
 * @param <V> The type of the second object.
 */
@RequiredArgsConstructor
@ToString
@EqualsAndHashCode
public class Pair<K, V> {

    /**
     * The first entry of the pair.
     */
    private @Getter final K key;

    /**
     * The second entry of the pair.
     */
    private @Getter final V value;

    /**
     * Fetches the first entry of the pair.
     *
     * @return The first entry.
     */
    public K getFirst() {
        return key;
    }

    /**
     * Fetches the second entry of the pair.
     *
     * @return The second entry.
     */
    public V getSecond() {
        return value;
    }
}
