/*
 * Copyright (c) 2019. All Rights Reserved.
 */

package de.fuchspfoten.fuchslib.util.function.recursive;

import java.util.function.Consumer;

/**
 * Recursive {@link java.util.function.Consumer}.
 *
 * @param <T> The first parameter type of the {@link java.util.function.Consumer}.
 */
@FunctionalInterface
public interface RecursiveConsumer<T> {

    /**
     * @see java.util.function.Consumer#accept(Object)
     */
    void accept(T t, Consumer<T> self);
}
