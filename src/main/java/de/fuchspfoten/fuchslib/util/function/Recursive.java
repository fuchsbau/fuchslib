/*
 * Copyright (c) 2019. All Rights Reserved.
 */

package de.fuchspfoten.fuchslib.util.function;

import de.fuchspfoten.fuchslib.util.function.recursive.RecursiveConsumer;

import java.util.function.Consumer;

/**
 * Represents a recursive {@link java.lang.FunctionalInterface}.
 */
public class Recursive<F> {

    /**
     * Creates a recursive {@link java.util.function.Consumer}.
     *
     * @param in  The {@link de.fuchspfoten.fuchslib.util.function.recursive.RecursiveConsumer} that is converted to a
     *            {@link java.util.function.Consumer}.
     * @param <T> The first type parameter of the {@link java.util.function.Consumer}.
     * @return The resulting {@link java.util.function.Consumer}.
     */
    public static <T> Consumer<T> consumer(final RecursiveConsumer<T> in) {
        final Recursive<Consumer<T>> rec = new Recursive<>();
        rec.functionalInterface = t -> in.accept(t, rec.functionalInterface);
        return rec.functionalInterface;
    }

    /**
     * The functional interface.
     */
    private F functionalInterface;
}
