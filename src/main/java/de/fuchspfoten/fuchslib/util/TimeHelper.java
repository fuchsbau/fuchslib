/*
 * Copyright (c) 2019. All Rights Reserved.
 */

package de.fuchspfoten.fuchslib.util;

import de.fuchspfoten.fuchslib.Messenger;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * Helper for time-related things.
 */
public final class TimeHelper {

    /**
     * Maps key characters representing time units to millisecond durations of these time units.
     */
    private static final Map<Character, Long> timeUnits = new HashMap<>();

    static {
        timeUnits.put('s', 1000L);
        timeUnits.put('m', 60 * 1000L);
        timeUnits.put('h', 60 * 60 * 1000L);
        timeUnits.put('d', 24 * 60 * 60 * 1000L);
        timeUnits.put('w', 7 * 24 * 60 * 60 * 1000L);
        timeUnits.put('M', 4 * 7 * 24 * 60 * 60 * 1000L);
        timeUnits.put('y', 52 * 7 * 24 * 60 * 60 * 1000L);
    }

    /**
     * Registers messages with the messenger API.
     */
    public static void registerMessages() {
        Messenger.register("fuchslib.timeHelper.now");
        Messenger.register("fuchslib.timeHelper.second");
        Messenger.register("fuchslib.timeHelper.seconds");
        Messenger.register("fuchslib.timeHelper.minute");
        Messenger.register("fuchslib.timeHelper.minutes");
        Messenger.register("fuchslib.timeHelper.hour");
        Messenger.register("fuchslib.timeHelper.hours");
        Messenger.register("fuchslib.timeHelper.day");
        Messenger.register("fuchslib.timeHelper.days");
        Messenger.register("fuchslib.timeHelper.week");
        Messenger.register("fuchslib.timeHelper.weeks");
        Messenger.register("fuchslib.timeHelper.month");
        Messenger.register("fuchslib.timeHelper.months");
        Messenger.register("fuchslib.timeHelper.year");
        Messenger.register("fuchslib.timeHelper.years");
    }

    /**
     * Parses a time difference given as a suffix-based time component string.
     *
     * @param source The source string.
     * @return The resulting time difference in milliseconds.
     */
    public static long parseTimeDifference(final String source) {
        final Collection<String> parts = new ArrayList<>();

        // Parse the parts of the string.
        final StringBuilder builder = new StringBuilder();
        for (final char c : source.toCharArray()) {
            builder.append(c);
            if (c < '0' || c > '9') {
                parts.add(builder.toString());
                builder.setLength(0);
            }
        }
        // Ignore the final builder content: If the sequence is not terminated by a non-numeric character it is invalid.

        // Remove invalid parts without a magnitude.
        parts.removeIf(s -> s.charAt(0) < '0' || s.charAt(0) > '9');

        // Determine the actual time delta.
        long delta = 0;
        for (final String part : parts) {
            final char keyChar = part.charAt(part.length() - 1);
            final long magnitude = Long.parseLong(part.substring(0, part.length() - 1));
            delta += timeUnits.getOrDefault(keyChar, 0L) * magnitude;
        }
        return delta;
    }

    /**
     * Formats the difference as a string.
     *
     * @param milliDifference The time difference in milliseconds.
     * @return The formatted time difference.
     */
    public static String formatTimeDifference(final long milliDifference) {
        if (milliDifference < 1000) {
            return Messenger.getFormat("fuchslib.timeHelper.now");
        }

        // Get the time differences.
        long accu = milliDifference / 1000;
        final long yearDifference = accu / (60 * 60 * 24 * 7 * 52L);
        accu %= 60 * 60 * 24 * 7 * 52L;
        final long monthDifference = accu / (60 * 60 * 24 * 7 * 4L);
        accu %= 60 * 60 * 24 * 7 * 4L;
        final long weekDifference = accu / (60 * 60 * 24 * 7L);
        accu %= 60 * 60 * 24 * 7L;
        final long dayDifference = accu / (60 * 60 * 24L);
        accu %= 60 * 60 * 24L;
        final long hourDifference = accu / (60 * 60L);
        accu %= 60 * 60L;
        final long minuteDifference = accu / 60L;
        accu %= 60L;
        final long secondDifference = accu;

        // Build the formatted difference.
        final Collection<String> partList = new ArrayList<>();
        if (yearDifference > 0) {
            partList.add(formatAmount("fuchslib.timeHelper.year", yearDifference));
        }
        if (monthDifference > 0) {
            partList.add(formatAmount("fuchslib.timeHelper.month", monthDifference));
        }
        if (weekDifference > 0) {
            partList.add(formatAmount("fuchslib.timeHelper.week", weekDifference));
        }
        if (dayDifference > 0) {
            partList.add(formatAmount("fuchslib.timeHelper.day", dayDifference));
        }
        if (hourDifference > 0) {
            partList.add(formatAmount("fuchslib.timeHelper.hour", hourDifference));
        }
        if (minuteDifference > 0) {
            partList.add(formatAmount("fuchslib.timeHelper.minute", minuteDifference));
        }
        if (secondDifference > 0) {
            partList.add(formatAmount("fuchslib.timeHelper.second", secondDifference));
        }

        return String.join(", ", partList);
    }

    /**
     * Formats the most significant time difference as a string. For example, a difference of one minute, 34 seconds is
     * formatted as one minute would be.
     *
     * @param milliDifference The time difference in milliseconds.
     * @return The formatted most significant time difference.
     */
    public static String formatMostSignificantTimeDifference(final long milliDifference) {
        if (milliDifference < 1000) {
            return Messenger.getFormat("fuchslib.timeHelper.now");
        }

        final long secondDifference = milliDifference / 1000;
        if (secondDifference < 60) {
            return formatAmount("fuchslib.timeHelper.second", secondDifference);
        }

        final long minuteDifference = secondDifference / 60;
        if (minuteDifference < 60) {
            return formatAmount("fuchslib.timeHelper.minute", minuteDifference);
        }

        final long hourDifference = minuteDifference / 60;
        if (hourDifference < 60) {
            return formatAmount("fuchslib.timeHelper.hour", hourDifference);
        }

        final long dayDifference = hourDifference / 24;
        if (dayDifference < 7) {
            return formatAmount("fuchslib.timeHelper.day", dayDifference);
        }

        final long weekDifference = dayDifference / 7;
        if (weekDifference < 4) {
            return formatAmount("fuchslib.timeHelper.week", weekDifference);
        }

        // Somehow time is too complicated. We determine both month and year difference and show year difference iff it
        // is not zero, as a month-year connection would cause 48-week years.
        // Known downside: 12 months (e.g. 48 weeks) is not a year, but instead, 12 months.
        final long yearDifference = weekDifference / 52;
        if (yearDifference > 0) {
            return formatAmount("fuchslib.timeHelper.year", yearDifference);
        }

        final long monthDifference = weekDifference / 4;
        return formatAmount("fuchslib.timeHelper.month", monthDifference);
    }

    /**
     * Formats the given amount using the given format. If the amount is not {@code 1} an 's' is appended to the format.
     *
     * @param fmt    The format.
     * @param amount The amount to format.
     * @return The formatted amount.
     */
    private static String formatAmount(final String fmt, final long amount) {
        if (amount == 1) {
            return String.format(Messenger.getFormat(fmt), amount);
        }
        return String.format(Messenger.getFormat(fmt + 's'), amount);
    }

    /**
     * Private constructor to prevent instance creation.
     */
    private TimeHelper() {
    }
}
