/*
 * Copyright (c) 2019. All Rights Reserved.
 */

package de.fuchspfoten.fuchslib.util.graph;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * A graph that has key-like node associations. All associations must be unique and immutable -- otherwise the behavior
 * is undefined.
 *
 * @param <T> The type of the node association.
 */
public class KeyGraph<T> implements Graph<T> {

    /**
     * The nodes of the graph.
     */
    private final List<Node<T>> nodes = new ArrayList<>();

    /**
     * A map that associates data with nodes.
     */
    private final Map<T, Node<T>> nodeMap = new HashMap<>();

    @Override
    public void addNode(final Node<T> node) {
        nodes.add(node);
        nodeMap.put(node.getPayload(), node);
    }

    @Override
    public Node<T> getNodeAt(final int index) {
        return nodes.get(index);
    }

    @Override
    public Node<T> getNodeWith(final T data) {
        return nodeMap.get(data);
    }

    @Override
    public int getNumNodes() {
        return nodes.size();
    }
}
