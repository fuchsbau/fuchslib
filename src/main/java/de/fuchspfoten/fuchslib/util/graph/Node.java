/*
 * Copyright (c) 2019. All Rights Reserved.
 */

package de.fuchspfoten.fuchslib.util.graph;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

/**
 * A node in a graph.
 *
 * @param <T> The associated object type.
 */
@RequiredArgsConstructor
public class Node<T> {

    /**
     * The associated payload object.
     */
    private @Getter final T payload;

    /**
     * The adjacent (out-)neighbors.
     */
    private final Collection<Node<T>> adjacent = new ArrayList<>();

    /**
     * Adds the given node as an adjacent node.
     *
     * @param other The adjacent node.
     */
    public void addAdjacentNode(final Node<T> other) {
        adjacent.add(other);
    }

    /**
     * Obtains the adjacent nodes of this node.
     *
     * @return A collection of adjacent nodes.
     */
    public Collection<Node<T>> getAdjacentNodes() {
        return Collections.unmodifiableCollection(adjacent);
    }
}
