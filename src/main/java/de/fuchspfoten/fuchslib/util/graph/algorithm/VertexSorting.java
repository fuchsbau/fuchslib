/*
 * Copyright (c) 2019. All Rights Reserved.
 */

package de.fuchspfoten.fuchslib.util.graph.algorithm;

import de.fuchspfoten.fuchslib.util.function.Recursive;
import de.fuchspfoten.fuchslib.util.graph.Graph;
import de.fuchspfoten.fuchslib.util.graph.Node;

import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.function.Consumer;

/**
 * Provides algorithms for vertex sorting.
 */
public final class VertexSorting {

    /**
     * Attempts to create a topological sorting of the nodes of the given graph.
     *
     * @param graph The graph to sort.
     * @param <T>   The associated data parameter of the graph.
     * @return The topologically sorted list of nodes.
     * @throws java.lang.IllegalArgumentException If the graph is no directed acyclic graph.
     */
    public static <T> List<Node<T>> topologicalSorting(final Graph<T> graph) {
        final List<Node<T>> result = new LinkedList<>();
        final Collection<Node<T>> closedSet = new HashSet<>();
        final Collection<Node<T>> openSet = new HashSet<>();
        final Consumer<Node<T>> visitor = Recursive.consumer((node, self) -> {
            if (closedSet.contains(node)) {
                return;
            }
            if (openSet.contains(node)) {
                throw new IllegalArgumentException("graph has at least one cycle");
            }
            openSet.add(node);
            for (final Node<T> adjacent : node.getAdjacentNodes()) {
                self.accept(adjacent);
            }
            openSet.remove(node);
            closedSet.add(node);
            result.add(0, node);
        });

        int closedBefore = 0;
        while (closedSet.size() < graph.getNumNodes()) {
            for (int i = closedBefore; i < graph.getNumNodes(); i++) {
                final Node<T> node = graph.getNodeAt(i);
                if (!closedSet.contains(node)) {
                    closedBefore = i;
                    visitor.accept(node);
                    break;
                }
            }
        }

        return result;
    }

    /**
     * Private constructor to prevent instance creation.
     */
    private VertexSorting() {
    }
}
