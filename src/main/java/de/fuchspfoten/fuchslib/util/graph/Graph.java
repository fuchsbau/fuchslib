/*
 * Copyright (c) 2019. All Rights Reserved.
 */

package de.fuchspfoten.fuchslib.util.graph;

/**
 * Represents a graph.
 *
 * @param <T> The node association object type.
 */
public interface Graph<T> {

    /**
     * Adds the given node to the graph.
     *
     * @param node The node to add.
     */
    void addNode(Node<T> node);

    /**
     * Returns the node at the given index.
     *
     * @param index The index.
     */
    Node<T> getNodeAt(int index);

    /**
     * Returns the node with the given data object (equals-comparison).
     *
     * @param data The data object.
     */
    Node<T> getNodeWith(T data);

    /**
     * Returns the number of nodes in the graph.
     *
     * @return The number of nodes in the graph.
     */
    int getNumNodes();
}
