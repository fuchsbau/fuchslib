/*
 * Copyright (c) 2018. All Rights Reserved.
 */

package de.fuchspfoten.fuchslib;

import org.bukkit.Bukkit;
import org.bukkit.Location;

/**
 * Utility class to serialize things into strings.
 */
public final class StringSerializer {

    /**
     * String to Location.
     *
     * @param s The String.
     * @return The Location.
     */
    public static Location parseLocation(final String s) {
        final String[] parts = s.split(";");
        return new Location(Bukkit.getWorld(parts[0]), Double.parseDouble(parts[1]), Double.parseDouble(parts[2]),
                Double.parseDouble(parts[3]), Float.parseFloat(parts[5]), Float.parseFloat(parts[4]));
    }

    /**
     * Location to String.
     *
     * @param location The Location.
     * @return The String.
     */
    public static String toString(final Location location) {
        return location.getWorld().getName() + ';' + location.getX() + ';' + location.getY() + ';' + location.getZ()
                + ';' + location.getPitch() + ';' + location.getYaw();
    }

    /**
     * Private constructor to prevent instance creation.
     */
    private StringSerializer() {
    }
}
