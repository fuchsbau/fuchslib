/*
 * Copyright (c) 2018. All Rights Reserved.
 */

package de.fuchspfoten.fuchslib.exceptions;

/**
 * Thrown when something is not applicable for the designated purpose.
 */
public class NotApplicableException extends RuntimeException {

    /**
     * Constructor.
     */
    public NotApplicableException() {
    }

    /**
     * Constructor.
     *
     * @param message The message for the exception.
     */
    public NotApplicableException(final String message) {
        super(message);
    }

    /**
     * Constructor.
     *
     * @param message The message for the exception.
     * @param cause   The cause of the exception.
     */
    public NotApplicableException(final String message, final Throwable cause) {
        super(message, cause);
    }

    /**
     * Constructor.
     *
     * @param cause The cause of the exception.
     */
    public NotApplicableException(final Throwable cause) {
        super(cause);
    }
}
