/*
 * Copyright (c) 2018. All Rights Reserved.
 */

package de.fuchspfoten.fuchslib.tests;

import org.junit.Assert;

/**
 * Helpers for tests.
 */
public final class TestHelper {

    /**
     * Asserts that the given code throws the given exception.
     *
     * @param code       The code.
     * @param exceptions The exception.
     * @return The thrown exception.
     */
    @SafeVarargs
    public static Throwable assertThrows(final Runnable code, final Class<? extends Throwable>... exceptions) {
        try {
            code.run();
        } catch (final Throwable all) {
            for (final Class<? extends Throwable> guard : exceptions) {
                if (guard.isAssignableFrom(all.getClass())) {
                    return all;
                }
            }
            throw all;
        }

        Assert.fail("Code does not throw!");
        throw new IllegalStateException("Failure");
    }

    /**
     * Private constructor to prevent instance creation.
     */
    private TestHelper() {
    }
}
