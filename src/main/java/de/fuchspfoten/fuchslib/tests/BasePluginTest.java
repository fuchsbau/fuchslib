/*
 * Copyright (c) 2018. All Rights Reserved.
 */

package de.fuchspfoten.fuchslib.tests;

import de.fuchspfoten.mokkit.Mokkit;
import de.fuchspfoten.mokkit.MokkitServer;
import org.bukkit.plugin.java.JavaPlugin;
import org.junit.After;
import org.junit.Before;

/**
 * A basic plugin test with junit.
 */
public class BasePluginTest<T extends JavaPlugin> {

    /**
     * The plugin class.
     */
    private final Class<T> pluginClass;

    /**
     * The plugin dependencies.
     */
    private final Class[] dependencies;

    /**
     * The mocked server.
     */
    protected MokkitServer server;

    /**
     * The mocked plugin.
     */
    protected T plugin;

    /**
     * Constructor.
     *
     * @param pluginClass  The plugin class.
     * @param dependencies The dependencies, in load order.
     */
    @SafeVarargs
    protected BasePluginTest(final Class<T> pluginClass, final Class<? extends JavaPlugin>... dependencies) {
        this.pluginClass = pluginClass;
        this.dependencies = dependencies.clone();
    }

    @Before
    public void baseBefore() {
        server = Mokkit.startServer();

        // Load dependencies.
        for (final Class<?> clazz : dependencies) {
            //noinspection unchecked
            Mokkit.loadPlugin((Class<? extends JavaPlugin>) clazz);
        }

        // Load plugin.
        plugin = Mokkit.loadPlugin(pluginClass);
    }

    @After
    public void baseAfter() {
        Mokkit.stopServer();
    }
}
