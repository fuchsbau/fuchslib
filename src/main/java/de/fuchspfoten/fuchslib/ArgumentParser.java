/*
 * Copyright (c) 2018. All Rights Reserved.
 */

package de.fuchspfoten.fuchslib;

import lombok.ToString;
import org.bukkit.command.CommandSender;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Helper for parsing UNIX-like arguments.
 */
@ToString
public class ArgumentParser {

    /**
     * Arguments which do not take a parameter.
     */
    private final Set<Character> switchArguments = new HashSet<>();

    /**
     * Arguments mapped to their descriptions.
     */
    private final Map<Character, String> argumentDescriptionMap = new HashMap<>();

    /**
     * The arguments that were present during the last parse, together with their values.
     */
    private final Map<Character, String> parseCache = new HashMap<>();

    /**
     * Constructor.
     */
    public ArgumentParser() {
        addSwitch('h', "Displays this help");
    }

    /**
     * Adds a switch.
     *
     * @param arg         The argument.
     * @param description The description of the argument.
     */
    public void addSwitch(final char arg, final String description) {
        if (argumentDescriptionMap.containsKey(arg)) {
            throw new IllegalArgumentException("already provided: " + arg);
        }
        argumentDescriptionMap.put(arg, "-" + arg + " -- " + description);
        switchArguments.add(arg);
    }

    /**
     * Adds an argument.
     *
     * @param arg         The argument.
     * @param description The description of the argument.
     * @param paramName   The name of the parameter.
     */
    public void addArgument(final char arg, final String description, final String paramName) {
        if (argumentDescriptionMap.containsKey(arg)) {
            throw new IllegalArgumentException("already provided: " + arg);
        }
        argumentDescriptionMap.put(arg, "-" + arg + " <" + paramName + "> -- " + description);
    }

    /**
     * Retrieves the value of the given argument from the previous parse. {@code null} if not provided.
     *
     * @param arg The argument.
     * @return The value or null. For switches, the empty string is the value.
     */
    public String getArgument(final char arg) {
        return parseCache.get(arg);
    }

    /**
     * Checks whether or not the given argument was included in the previous parse.
     *
     * @param arg The argument.
     * @return Whether the argument was provided.
     */
    public boolean hasArgument(final char arg) {
        return getArgument(arg) != null;
    }

    /**
     * Shows the help to a target.
     *
     * @param target The target that will receive the help.
     * @param format The format for help lines. Takes one string, the help line.
     * @deprecated Use {@link de.fuchspfoten.fuchslib.ArgumentParser#showHelp(org.bukkit.command.CommandSender)} instead
     */
    @Deprecated
    public void showHelp(final CommandSender target, final String format) {
        for (final String line : argumentDescriptionMap.values()) {
            target.sendMessage(String.format(format, line));
        }
    }

    /**
     * Shows the help to a target.
     *
     * @param target The target that will receive the help.
     */
    public void showHelp(final CommandSender target) {
        for (final String line : argumentDescriptionMap.values()) {
            Messenger.send(target, "commandHelp", line);
        }
    }

    /**
     * Parses an argument array.
     *
     * @param args The argument array.
     * @return The argument array without the UNIX-like arguments.
     */
    public String[] parse(final String[] args) {
        // Figure out where the payload starts and separate the two components of the arguments.
        int payloadStart = args.length;
        boolean expectingParam = false;
        for (int i = 0; i < args.length; i++) {
            if (args[i].equals("--")) {
                payloadStart = i + 1;
                break;
            }

            // Check whether or not the current entry might be an argument.
            final boolean isArg = args[i].length() > 1 && args[i].charAt(0) == '-';

            if (!isArg && !expectingParam) {
                // Not an argument, not expecting a parameter: Argument list ended.
                payloadStart = i;
                break;
            }

            if (expectingParam) {
                // Expecting a parameter, take the parameter and continue.
                expectingParam = false;
                continue;
            }

            // Check whether the argument takes a parameter.
            final char arg = args[i].charAt(args[i].length() - 1);
            if (!switchArguments.contains(arg) && argumentDescriptionMap.containsKey(arg)) {
                // Argument takes a parameter.
                expectingParam = true;
            }
        }

        // Copy the payload and the args seperately.
        final String[] arguments = Arrays.copyOfRange(args, 0, payloadStart);
        final String[] payload = Arrays.copyOfRange(args, payloadStart, args.length);

        // Clear the previous parse.
        parseCache.clear();

        // Interpret the arguments.
        boolean parserExpectingParam = false;
        char parameterStore = '-';
        for (final String arg : arguments) {
            if (parserExpectingParam) {
                parseCache.put(parameterStore, arg);
            } else {
                // The last argument flag receives the parameter.
                parameterStore = arg.charAt(arg.length() - 1);
                parserExpectingParam = !switchArguments.contains(parameterStore);

                // Initialize the cache for all arguments.
                for (final char charArg : arg.substring(1).toCharArray()) {
                    if (argumentDescriptionMap.containsKey(charArg)) {
                        parseCache.put(charArg, "");
                    }
                }
            }
        }

        return payload;
    }
}
