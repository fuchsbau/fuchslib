/*
 * Copyright (c) 2018. All Rights Reserved.
 */

package de.fuchspfoten.fuchslib;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Provides utilities for messaging.
 */
public final class Messenger {

    /**
     * Cache of already retrieved formats.
     */
    private static final Map<String, String> formatCache = new HashMap<>();

    /**
     * The backend configuration.
     */
    private static YamlConfiguration formatStorage;

    /**
     * The backend file.
     */
    private static File formatBackend;

    /**
     * Loads the message configuration from a file.
     *
     * @param backend The file.
     */
    public static void load(final File backend) {
        formatBackend = backend;
        formatStorage = YamlConfiguration.loadConfiguration(backend);
        for (final String key : formatStorage.getKeys(true)) {
            if (!formatStorage.isString(key)) {
                continue;
            }
            formatCache.put(key, formatStorage.getString(key));
        }
    }

    /**
     * Registers a message.
     *
     * @param key The key.
     */
    public static void register(final String key) {
        if (!formatStorage.contains(key)) {
            formatStorage.set(key, key);
            formatCache.put(key, key);
            save();
        }
    }

    /**
     * Sends a message to a target.
     * <p>
     * <b>This method is thread safe.</b>
     *
     * @param sender The target.
     * @param key    The message key.
     * @param data   The data to be inserted into the format string.
     */
    public static void send(final CommandSender sender, final String key, final Object... data) {
        final Runnable sandbox = () -> {
            final String fmt = formatCache.get(key);
            if (fmt == null) {
                FuchsLibPlugin.getSelf().getLogger().severe("Attempted to send null message: " + key);
                sender.sendMessage("null");
                return;
            }
            sender.sendMessage(String.format(formatCache.get(key), data));
        };

        // Ensure thread safety.
        if (Bukkit.isPrimaryThread()) {
            // Guaranteed to be safe.
            sandbox.run();
        } else {
            // Might be unsafe, err on the side of caution.
            Bukkit.getScheduler().scheduleSyncDelayedTask(FuchsLibPlugin.getSelf(), sandbox);
        }
    }

    /**
     * Returns the format string with the given key.
     *
     * @param key The key.
     * @return The format string.
     */
    public static String getFormat(final String key) {
        return formatCache.get(key);
    }

    /**
     * Saves the message configuration.
     */
    private static void save() {
        try {
            formatStorage.save(formatBackend);
        } catch (final IOException e) {
            FuchsLibPlugin.getSelf().getLogger().severe("Could not save messages.yml");
            e.printStackTrace();
        }
    }

    /**
     * Private constructor to prevent instance creation.
     */
    private Messenger() {
    }
}
