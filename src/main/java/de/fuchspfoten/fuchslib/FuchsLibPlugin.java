/*
 * Copyright (c) 2019. All Rights Reserved.
 */

package de.fuchspfoten.fuchslib;

import de.fuchspfoten.fuchslib.data.PlayerData;
import de.fuchspfoten.fuchslib.data.UUIDLookup;
import de.fuchspfoten.fuchslib.internal.BannedItemDeleter;
import de.fuchspfoten.fuchslib.internal.CustomItemStacker;
import de.fuchspfoten.fuchslib.internal.DiamondHoeDisabler;
import de.fuchspfoten.fuchslib.internal.SelectorWheelRegistry;
import de.fuchspfoten.fuchslib.internal.command.FuchslibCommand;
import de.fuchspfoten.fuchslib.multiblock.MachineRegistry;
import de.fuchspfoten.fuchslib.ui.SelectorWheel;
import de.fuchspfoten.fuchslib.util.TimeHelper;
import lombok.Getter;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.io.IOException;

/**
 * Plugin for being loaded by Bukkit.
 */
public class FuchsLibPlugin extends JavaPlugin implements Listener {

    /**
     * Singleton plugin instance.
     */
    private @Getter static FuchsLibPlugin self;

    /**
     * The selector wheel registry.
     */
    private @Getter SelectorWheelRegistry selectorWheelRegistry;

    @EventHandler
    public void onPlayerLogin(final PlayerLoginEvent event) {
        UUIDLookup.provide(event.getPlayer().getName(), event.getPlayer().getUniqueId());
    }

    @EventHandler
    public void onPlayerQuit(final PlayerQuitEvent event) {
        final PlayerData pd = new PlayerData(event.getPlayer());
        pd.save();
    }

    @Override
    public void onDisable() {
        getLogger().info("Saving all player storages...");
        PlayerData.saveAll();
        getLogger().info("Done!");

        getLogger().info("Saving the UUID lookup table...");
        UUIDLookup.save();
        getLogger().info("Done!");
    }

    /**
     * Reloads the registered messages.
     */
    public void reloadMessages() {
        final File messengerFile = new File(getDataFolder(), "messages.yml");
        if (!messengerFile.exists()) {
            try {
                if (!messengerFile.createNewFile()) {
                    throw new IllegalStateException("Could not create messages.yml: failure");
                }
            } catch (final IOException ex) {
                throw new IllegalStateException("Could not create messages.yml", ex);
            }
        }
        Messenger.load(messengerFile);
    }

    @Override
    public void onEnable() {
        self = this;

        // Create default config.
        getConfig().options().copyDefaults(true);
        saveConfig();

        // Setup the UUID lookup.
        final File uuidLookupFile = new File(getDataFolder(), "uuidtable.yml");
        if (!uuidLookupFile.exists()) {
            try {
                if (!uuidLookupFile.createNewFile()) {
                    throw new IllegalStateException("Could not create UUID table: Failure");
                }
            } catch (final IOException e) {
                throw new IllegalStateException("Could not create UUID table", e);
            }
        }
        UUIDLookup.load(uuidLookupFile);

        // Load and create the messenger configuration, if needed.
        reloadMessages();
        Messenger.register("commandHelp");
        Messenger.register("treeCommandNotFound");

        // Register messages.
        PlayerHelper.registerMessages();
        TimeHelper.registerMessages();

        // Initialize classes.
        selectorWheelRegistry = new SelectorWheelRegistry();
        SelectorWheel.initialize(getConfig().getConfigurationSection("selectorWheel"));

        // Create the playerdata folder, if needed.
        final File playerDataFolder = new File(getDataFolder(), "playerdata");
        if (!playerDataFolder.exists()) {
            if (!playerDataFolder.mkdir()) {
                throw new IllegalStateException("Could not create player data folder!");
            }
        }

        // Register commands.
        getCommand("fuchslib").setExecutor(new FuchslibCommand());

        // Register listeners.
        getServer().getPluginManager().registerEvents(this, this);
        getServer().getPluginManager().registerEvents(new BannedItemDeleter(), this);
        getServer().getPluginManager().registerEvents(new CustomItemStacker(), this);
        getServer().getPluginManager().registerEvents(new DiamondHoeDisabler(), this);
        getServer().getPluginManager().registerEvents(new MachineRegistry(), this);
        getServer().getPluginManager().registerEvents(selectorWheelRegistry, this);

        // Create periodic task of saving online users.
        getServer().getScheduler().scheduleSyncRepeatingTask(this, () -> {
            for (final Player online : Bukkit.getOnlinePlayers()) {
                final PlayerData pd = new PlayerData(online);
                pd.save();
            }
        }, 60 * 20L, 60 * 20L);

        // Setup pagination.
        Pagination.registerFormats();
    }
}
