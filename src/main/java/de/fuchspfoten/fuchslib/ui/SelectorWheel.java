/*
 * Copyright (c) 2019. All Rights Reserved.
 */

package de.fuchspfoten.fuchslib.ui;

import de.fuchspfoten.fuchslib.FuchsLibPlugin;
import de.fuchspfoten.fuchslib.Messenger;
import de.fuchspfoten.fuchslib.item.HeadUtils;
import de.fuchspfoten.fuchslib.spectre.ArmorStandSpectre;
import de.fuchspfoten.fuchslib.spectre.Spectre;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.bukkit.Location;
import org.bukkit.Particle;
import org.bukkit.Sound;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.util.Consumer;
import org.bukkit.util.Vector;

import java.util.Arrays;
import java.util.Collections;
import java.util.Random;
import java.util.UUID;

/**
 * A selector wheel provides the user with a selection.
 */
public class SelectorWheel {

    /**
     * The heads to display on the selector wheel.
     */
    private static ItemStack[] heads;

    /**
     * Initializes selector wheel items and messages.
     *
     * @param config The configuration section.
     */
    public static void initialize(final ConfigurationSection config) {
        Messenger.register("fuchslib.selectorWheel.hint");
        heads = new ItemStack[10];
        for (int i = 0; i < 10; i++) {
            final ConfigurationSection headSection = config.getConfigurationSection(String.valueOf(i));
            heads[i] = HeadUtils.getFromUUIDAndTexture(UUID.fromString(headSection.getString("uuid")),
                    headSection.getString("texture"));
        }
    }

    /**
     * The options of the selector wheel.
     */
    private SelectorWheelOption[] options;

    /**
     * The spectres displaying the options.
     */
    private ArmorStandSpectre[] spectres;

    /**
     * The center location.
     */
    private @Getter Location center;

    /**
     * The target player.
     */
    private Player target;

    /**
     * The currently highlighted option.
     */
    private int optionHighlighted = -1;

    /**
     * Sets the options for the selector wheel.
     *
     * @param options The options on the wheel.
     * @return {@code this}, for chaining.
     */
    public SelectorWheel options(final SelectorWheelOption... options) {
        if (this.options == null) {
            this.options = new SelectorWheelOption[options.length + 1];
        } else if (this.options.length - 1 < options.length) {
            final SelectorWheelOption[] data = new SelectorWheelOption[options.length + 1];
            data[0] = this.options[0];
            this.options = data;
        }
        System.arraycopy(options, 0, this.options, 1, options.length);
        return this;
    }

    /**
     * Sets the cancellation option for the selector wheel.
     *
     * @param cancelOption The cancellation option.
     * @return {@code this}, for chaining.
     */
    public SelectorWheel cancelOption(final SelectorWheelOption cancelOption) {
        if (options == null) {
            options = new SelectorWheelOption[1];
        }
        options[0] = cancelOption;
        return this;
    }

    /**
     * Shows the selector wheel to the given target and registers it.
     *
     * @param target The target.
     */
    public void show(final Player target) {
        if (spectres != null) {
            throw new IllegalArgumentException("currently being shown!");
        }
        this.target = target;
        center = target.getLocation();

        // Create the actual spectre wheel.
        createWheel();
        turn(target.getLocation().getDirection());

        // Add a blindness effect.
        target.addPotionEffect(new PotionEffect(PotionEffectType.BLINDNESS, Integer.MAX_VALUE / 2, 0,
                true, false));

        // Register the wheel and show a hint.
        FuchsLibPlugin.getSelf().getSelectorWheelRegistry().registerSelectorWheel(target.getUniqueId(), this);
        Messenger.send(target, "fuchslib.selectorWheel.hint");
    }

    /**
     * Hides the selection wheel.
     *
     * @param cancel Invokes the cancellation option iff {@code true}.
     */
    public void hide(final boolean cancel) {
        // Perform the cancellation action if requested.
        if (cancel) {
            options[0].getOnSelect().accept(spectres[0].getLocation());
            playCancelEffects();
        }

        // Remove the spectres from the wheel.
        Arrays.stream(spectres).forEach(Spectre::remove);
        spectres = null;

        // Remove the blindness effect.
        target.removePotionEffect(PotionEffectType.BLINDNESS);

        // Unregister the selector wheel.
        FuchsLibPlugin.getSelf().getSelectorWheelRegistry().unregisterWheel(target.getUniqueId());
    }

    /**
     * Selects the currently highlighted action.
     */
    public void select() {
        if (optionHighlighted != -1) {
            options[optionHighlighted].getOnSelect().accept(spectres[optionHighlighted].getLocation());
            if (optionHighlighted == 0) {
                playCancelEffects();
            }
            hide(false);
        }
    }

    /**
     * Signals a turn in the given direction and selects the appropriate option.
     *
     * @param direction The direction.
     */
    public void turn(final Vector direction) {
        int newHighlight = -1;
        final double testAngle = 0.5 * Math.PI / options.length;
        for (int i = 0; i < spectres.length; i++) {
            final Vector spectreDirection = spectres[i].getLocation().getDirection().clone().multiply(-1);
            final double angle = spectreDirection.angle(direction);
            if (Math.abs(angle) <= testAngle) {
                newHighlight = i;
                break;
            }
        }

        if (newHighlight != optionHighlighted) {
            // Move target a bit up.
            if (newHighlight != -1) {
                spectres[newHighlight].move(0.0, 0.25, 0.0);
                target.playSound(spectres[newHighlight].getLocation(), Sound.UI_BUTTON_CLICK, 0.5f, 0.0f);
            }

            // Move old target back.
            if (optionHighlighted != -1) {
                spectres[optionHighlighted].move(0.0, -0.25, 0.0);
                target.playSound(spectres[optionHighlighted].getLocation(), Sound.ITEM_ARMOR_EQUIP_LEATHER,
                        0.5f, 0.0f);
            }

            optionHighlighted = newHighlight;
        }
    }

    /**
     * Plays cancellation effects.
     */
    private void playCancelEffects() {
        final Random random = new Random();
        Arrays.stream(spectres).forEach(s -> {
            target.spawnParticle(Particle.LAVA, s.getLocation().add(0, 0.5, 0), 20, 0.25,
                    0.3, 0.25, 1.0);
            target.playSound(s.getLocation(), Sound.BLOCK_LAVA_EXTINGUISH, 0.5f,
                    1.0f + 0.4f * random.nextFloat());
        });
    }

    /**
     * Creates the actual selection spectre wheel.
     */
    private void createWheel() {
        final double angle = Math.PI / options.length;
        final double sinAngle = Math.sin(angle);
        final double cosAngle = Math.cos(angle);
        final double sinHalfAngle = Math.sin(angle * 0.5);
        final double cosHalfAngle = Math.cos(angle * 0.5);
        final Vector front = center.getDirection().setY(0).normalize();

        // Obtain the cursor as the left-vector rotated half an angle back.
        Vector cursor = new Vector(front.getZ() * 3.0, 0.0, -front.getX() * 3.0);
        cursor = new Vector(cursor.getX() * cosHalfAngle + cursor.getZ() * sinHalfAngle, 0.0,
                -cursor.getX() * sinHalfAngle + cursor.getZ() * cosHalfAngle);

        // Show the selection spectres.
        spectres = new ArmorStandSpectre[options.length];
        for (int i = 0; i < options.length; i++) {
            cursor = new Vector(cursor.getX() * cosAngle - cursor.getZ() * sinAngle, 0.0,
                    cursor.getX() * sinAngle + cursor.getZ() * cosAngle);
            final Location pos = target.getLocation().add(cursor).add(0.0, -0.5, 0.0);
            pos.setDirection(new Vector(-cursor.getX(), 0.0, -cursor.getZ()));
            spectres[i] = new ArmorStandSpectre(pos, Collections.singleton(target));
            spectres[i].setInvisible(true);
            spectres[i].setHasBasePlate(false);
            spectres[i].setHelmet(heads[i]);
            spectres[i].setNameTag(options[i].getCaption());
        }
    }

    @RequiredArgsConstructor
    public static class SelectorWheelOption {

        /**
         * The caption of the option.
         */
        private @Getter final String caption;

        /**
         * The selector callback. Receives the location of the selected spectre.
         */
        private @Getter final Consumer<Location> onSelect;
    }
}
