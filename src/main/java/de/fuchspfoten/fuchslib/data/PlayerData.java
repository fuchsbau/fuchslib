/*
 * Copyright (c) 2018. All Rights Reserved.
 */

package de.fuchspfoten.fuchslib.data;

import de.fuchspfoten.fuchslib.FuchsLibPlugin;
import lombok.Getter;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;

/**
 * Represents a data storage for a player.
 */
public class PlayerData {

    /**
     * Cache for UUID -> player data.
     */
    private static final Map<UUID, YamlConfiguration> dataCache = new HashMap<>();

    /**
     * Saves all cached configurations.
     */
    public static void saveAll() {
        for (final Entry<UUID, YamlConfiguration> entry : dataCache.entrySet()) {
            try {
                entry.getValue().save(getFileForUUID(entry.getKey()));
            } catch (final IOException e) {
                FuchsLibPlugin.getSelf().getLogger().severe("Could not save storage of " + entry.getKey());
                e.printStackTrace();
            }
        }
    }

    /**
     * Fetches a file for the given UUID to store data in.
     *
     * @param uuid The UUID.
     * @return The file.
     */
    private static File getFileForUUID(final UUID uuid) {
        return new File(FuchsLibPlugin.getSelf().getDataFolder(), "playerdata/" + uuid + ".yml");
    }

    /**
     * The YAML configuration containing the player's data.
     */
    private @Getter final YamlConfiguration storage;

    /**
     * The owner of this data storage.
     */
    private final UUID owner;

    /**
     * Creates a player data object for the given player name. Loads it from file if it exists.
     *
     * @param playerName The name of the player.
     */
    public PlayerData(final String playerName) {
        this(UUIDLookup.lookup(playerName));
    }

    /**
     * Creates a player data object for the given player. Loads it from file if it exists.
     *
     * @param player The player.
     */
    public PlayerData(final OfflinePlayer player) {
        // Fetch the configuration.
        storage = dataCache.computeIfAbsent(player.getUniqueId(),
                u -> YamlConfiguration.loadConfiguration(getFileForUUID(u)));
        owner = player.getUniqueId();
    }

    /**
     * Creates a player data object for the given UUID. Loads it from file if it exists.
     *
     * @param uuid The UUID of the player.
     */
    public PlayerData(final UUID uuid) {
        this(Bukkit.getOfflinePlayer(uuid));
    }

    /**
     * Sets the given flag for this player data.
     *
     * @param flag The flag.
     */
    public void setFlag(final String flag) {
        storage.set("flags." + flag, true);
    }

    /**
     * Clears the given flag for this player data.
     *
     * @param flag The flag.
     */
    public void clearFlag(final String flag) {
        storage.set("flags." + flag, false);
    }

    /**
     * Toggles the given flag for this player data.
     *
     * @param flag The flag.
     */
    public void toggleFlag(final String flag) {
        if (hasFlag(flag)) {
            clearFlag(flag);
        } else {
            setFlag(flag);
        }
    }

    /**
     * Checks whether the given flag is set in this player data.
     *
     * @param flag The flag.
     * @return Whether the given flag is set.
     */
    public boolean hasFlag(final String flag) {
        return storage.getBoolean("flags." + flag, false);
    }

    /**
     * Modifies the double stored at the given location. If the double does not exist, it is created with value 0.
     *
     * @param path       The path to the double.
     * @param alteration The additive alteration value.
     * @return The old value of the double.
     */
    public double modifyDouble(final String path, final double alteration) {
        return modifyDouble(path, alteration, 0);
    }

    /**
     * Modifies the double stored at the given location.
     *
     * @param path         The path to the double.
     * @param alteration   The additive alteration value.
     * @param defaultValue The default value in case the double does not exist.
     * @return The old value of the double.
     */
    public double modifyDouble(final String path, final double alteration, final double defaultValue) {
        final double old = storage.getDouble(path, defaultValue);
        storage.set(path, old + alteration);
        return old;
    }

    /**
     * Saves the data storage.
     */
    public void save() {
        try {
            storage.save(getFileForUUID(owner));
        } catch (final IOException e) {
            FuchsLibPlugin.getSelf().getLogger().severe("Could not save storage of " + owner);
            e.printStackTrace();
        }
    }
}
