/*
 * Copyright (c) 2019. All Rights Reserved.
 */

package de.fuchspfoten.fuchslib.data;

import de.fuchspfoten.fuchslib.FuchsLibPlugin;
import org.bukkit.Bukkit;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * Provides a UUID lookup table.
 */
public final class UUIDLookup {

    private static final Map<String, UUID> nameCache = new HashMap<>();

    /**
     * The file in which the lookup table is stored.
     */
    private static File lookupFile;

    /**
     * The YAML configuration in which the lookup table is stored.
     */
    private static YamlConfiguration lookupStorage;

    /**
     * Loads the lookup table from a file.
     *
     * @param storage The file.
     */
    public static void load(final File storage) {
        if (lookupFile != null) {
            throw new IllegalStateException("reinitializing!");
        }
        lookupFile = storage;
        lookupStorage = YamlConfiguration.loadConfiguration(lookupFile);
        for (final String nameKey : lookupStorage.getKeys(false)) {
            nameCache.put(nameKey, UUID.fromString(lookupStorage.getString(nameKey)));
        }
    }

    /**
     * Saves the lookup table.
     */
    public static void save() {
        try {
            lookupStorage.save(lookupFile);
        } catch (final IOException e) {
            FuchsLibPlugin.getSelf().getLogger().severe("Could not store UUID lookup table!");
            e.printStackTrace();
        }
    }

    /**
     * Performs an UUID lookup for the given player name. This might block.
     *
     * @param playerName The name of the player.
     * @return The UUID of the player.
     */
    public static UUID lookup(final String playerName) {
        final String lowerName = playerName.toLowerCase();
        if (nameCache.containsKey(lowerName)) {
            return nameCache.get(lowerName);
        }
        final UUID id = Bukkit.getOfflinePlayer(lowerName).getUniqueId();
        nameCache.put(lowerName, id);
        lookupStorage.set(lowerName, id.toString());
        return id;
    }

    /**
     * Performs an UUID lookup for the given player name.
     *
     * @param playerName The name of the player.
     * @return The UUID of the player, or {@code null} if not found.
     */
    public static UUID lookupNonBlocking(final String playerName) {
        return nameCache.get(playerName.toLowerCase());
    }

    /**
     * Provides the lookup table with a name / UUID pair.
     *
     * @param playerName The name of the player.
     * @param id         The UUID of the player.
     */
    public static void provide(final String playerName, final UUID id) {
        final String lowerName = playerName.toLowerCase();
        nameCache.put(lowerName, id);
        lookupStorage.set(lowerName, id.toString());
    }

    /**
     * Private constructor to prevent instance creation.
     */
    private UUIDLookup() {
    }
}
