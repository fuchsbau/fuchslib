/*
 * Copyright (c) 2019. All Rights Reserved.
 */

package de.fuchspfoten.fuchslib.data;

import lombok.Getter;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.IOException;
import java.util.function.BiConsumer;

/**
 * A wrapper for a YAML configuration with an associated file.
 */
public class DataFile {

    /**
     * The backend file.
     */
    private final File backend;

    /**
     * A consumer that updates the version of this data file.
     */
    private final BiConsumer<Integer, DataFile> versionUpdater;

    /**
     * The parsed storage as a YAML configuration.
     */
    private @Getter YamlConfiguration storage;

    /**
     * Constructor.
     *
     * @param backend           The path of the data file.
     * @param versionUpdater A consumer that updates the version of this data file.
     */
    public DataFile(final File backend, final BiConsumer<Integer, DataFile> versionUpdater) {
        // Ensure the backend exists.
        this.backend = backend;
        if (!this.backend.exists()) {
            try {
                if (!this.backend.createNewFile()) {
                    throw new IllegalStateException("could not create " + backend + ": failure");
                }
            } catch (final IOException ex) {
                throw new IllegalStateException("could not create " + backend, ex);
            }
        }
        this.versionUpdater = versionUpdater;

        reload();
    }

    /**
     * Reloads the data file from its backend.
     */
    public void reload() {
        // Load and parse the configuration.
        storage = YamlConfiguration.loadConfiguration(backend);

        // Perform a version conversion.
        versionUpdater.accept(storage.getInt("_version", -1), this);
    }

    /**
     * Saves this data file.
     *
     * @param version The version of the data file.
     */
    public void save(final int version) {
        // Change the version.
        storage.set("_version", version);

        // Save to disk.
        try {
            storage.save(backend);
        } catch (final IOException ex) {
            throw new IllegalStateException("could not save " + backend, ex);
        }
    }

    /**
     * Deletes the data file. The file may not be used afterwards.
     */
    public void delete() {
        if (!backend.delete()) {
            throw new IllegalStateException("could not delete " + backend);
        }
    }
}
