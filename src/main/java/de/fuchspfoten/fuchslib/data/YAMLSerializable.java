/*
 * Copyright (c) 2019. All Rights Reserved.
 */

package de.fuchspfoten.fuchslib.data;

import org.bukkit.configuration.ConfigurationSection;

/**
 * Represents classes that can be serialized to/from YAML.
 */
public interface YAMLSerializable {

    /**
     * Serializes the instance to the given target section.
     *
     * @param targetSection The target section.
     */
    void save(ConfigurationSection targetSection);

    /**
     * Loads the instance from the given source section. Classes may require to be initialized with source data
     * beforehand, e.g. for initializing final members. This initialization must, however, happen via the constructor.
     *
     * @param sourceSection The source section.
     */
    void load(ConfigurationSection sourceSection);
}
