/*
 * Copyright (c) 2019. All Rights Reserved.
 */

package de.fuchspfoten.fuchslib;

import de.fuchspfoten.fuchslib.data.UUIDLookup;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.UUID;

/**
 * Helper for fetching players.
 */
public final class PlayerHelper {

    /**
     * Registers messages with the messenger API.
     */
    public static void registerMessages() {
        Messenger.register("fuchslib.playerHelper.notExisting");
        Messenger.register("fuchslib.playerHelper.notOnline");
        Messenger.register("fuchslib.playerHelper.notSeen");
    }

    /**
     * Fetches a player by their name. If the player is not online, the method throws an IllegalArgumentException.
     *
     * @param name The name of the player.
     * @return The player.
     */
    public static Player requirePlayerByName(final String name) {
        final Player player = Bukkit.getPlayer(name);
        if (player == null || !player.isOnline()) {
            throw new IllegalArgumentException("player " + name + " not online!");
        }
        return player;
    }

    /**
     * Fetches a player by their name. If the player is not online, the method informs the given issuer.
     *
     * @param issuer The issuer of the action that requires a player.
     * @param name   The name of the player.
     * @return The player.
     */
    public static Player requirePlayerByName(final CommandSender issuer, final String name) {
        try {
            return requirePlayerByName(name);
        } catch (final IllegalArgumentException ex) {
            Messenger.send(issuer, "fuchslib.playerHelper.notOnline");
            return null;
        }
    }

    /**
     * Fetches a player by their name. If the player is not online or invisible, the method informs the given issuer.
     *
     * @param sender The issuer of the action that requires a player.
     * @param name   The name of the player.
     * @return The player.
     */
    public static Player requireVisiblePlayerByName(final Player sender, final String name) {
        final Player found = requirePlayerByName(sender, name);
        if (found != null && !sender.canSee(found)) {
            Messenger.send(sender, "fuchslib.playerHelper.notOnline");
            return null;
        }
        return found;
    }

    /**
     * Fetches an offline player by their name. If the player does not exist, the method throws an
     * IllegalArgumentException.
     * <p>
     * This might block.
     *
     * @param name The name.
     * @return The player.
     */
    public static OfflinePlayer requireOfflinePlayerByName(final String name) {
        final UUID uid = UUIDLookup.lookup(name);
        if (uid == null) {
            throw new IllegalArgumentException("player " + name + " not existing!");
        }
        return Bukkit.getOfflinePlayer(uid);
    }

    /**
     * Fetches an offline player by their name. If the player does not exist, the method informs the given issuer.
     *
     * @param issuer The issuer of the action that requires a player.
     * @param name   The name of the player.
     * @return The player.
     */
    public static OfflinePlayer requireOfflinePlayerByName(final CommandSender issuer, final String name) {
        try {
            return requireOfflinePlayerByName(name);
        } catch (final IllegalArgumentException ex) {
            Messenger.send(issuer, "fuchslib.playerHelper.notExisting");
            return null;
        }
    }

    /**
     * Fetches an offline player by their name. If the player was never seen, the method throws an
     * IllegalArgumentException.
     *
     * @param name The name.
     * @return The player.
     */
    public static OfflinePlayer requireSeenOfflinePlayerByName(final String name) {
        final UUID uid = UUIDLookup.lookupNonBlocking(name);
        if (uid == null) {
            throw new IllegalArgumentException("player " + name + " not seen!");
        }
        return Bukkit.getOfflinePlayer(uid);
    }

    /**
     * Fetches an offline player by their name. If the player was never seen, the method informs the given issuer.
     *
     * @param issuer The issuer of the action that requires a player.
     * @param name   The name of the player.
     * @return The player.
     */
    public static OfflinePlayer requireSeenOfflinePlayerByName(final CommandSender issuer, final String name) {
        try {
            return requireSeenOfflinePlayerByName(name);
        } catch (final IllegalArgumentException ex) {
            Messenger.send(issuer, "fuchslib.playerHelper.notSeen");
            return null;
        }
    }

    /**
     * Ensures that the given command sender is a player.
     *
     * @param commandSender The command sender.
     * @return The command sender cast to a player, or null if the command sender is no player.
     */
    public static Player ensurePlayer(final CommandSender commandSender) {
        if (!(commandSender instanceof Player)) {
            commandSender.sendMessage("This operation requires a player but you are "
                    + commandSender.getClass().getName());
            return null;
        }
        return (Player) commandSender;
    }

    /**
     * Private constructor to prevent instance creation.
     */
    private PlayerHelper() {
    }
}
