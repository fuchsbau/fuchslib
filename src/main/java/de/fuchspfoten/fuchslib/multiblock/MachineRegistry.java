/*
 * Copyright (c) 2018. All Rights Reserved.
 */

package de.fuchspfoten.fuchslib.multiblock;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;

import java.util.Collection;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * The registry which manages machines and their nodes.
 */
public class MachineRegistry implements Listener {

    /**
     * The registered machine factories. Maps materials to possible factories.
     */
    private static final Map<Material, Set<MachineFactory>> factoryRegistry = new EnumMap<>(Material.class);

    /**
     * Single block machines. These are handled seperately as a fast path.
     */
    private static final Map<Material, Set<SingleBlockMachine>> singleBlockRegistry = new EnumMap<>(Material.class);

    /**
     * Registers a factory to listen for the given materials.
     *
     * @param factory   The factory to register.
     * @param materials The materials to register it for.
     */
    public static void registerFactory(final MachineFactory factory, final Material... materials) {
        for (final Material material : materials) {
            factoryRegistry.computeIfAbsent(material, x -> new HashSet<>()).add(factory);
        }
    }

    /**
     * Registers a single block machine for the given materials.
     *
     * @param machine   The single block machine.
     * @param materials The materials to register it for.
     */
    public static void registerSingleBlockMachine(final SingleBlockMachine machine, final Material... materials) {
        for (final Material material : materials) {
            singleBlockRegistry.computeIfAbsent(material, x -> new HashSet<>()).add(machine);
        }
    }

    /**
     * Maps machines to their nodes, for O(1) access to machines starting from node blocks.
     */
    private final Map<Block, Machine> machineRegistry = new HashMap<>();

    @EventHandler
    public void onNodeInteract(final PlayerInteractEvent event) {
        // Only consider non-air blocks.
        if (event.getAction() != Action.RIGHT_CLICK_BLOCK && event.getAction() != Action.LEFT_CLICK_BLOCK) {
            return;
        }
        final Block block = event.getClickedBlock();

        // Check if the node is an existing node of a machine.
        final Machine machine = machineRegistry.get(block);
        if (machine != null) {
            machine.onNodeInteract(block, event);
            return;
        }

        // Check if a new machine can be created.
        final Collection<MachineFactory> possibleFactories = factoryRegistry.get(block.getType());
        if (possibleFactories != null) {
            for (final MachineFactory factory : possibleFactories) {
                // Attempt to create a machine.
                final Machine possibleCreated = factory.tryCreateAt(block);
                if (possibleCreated == null) {
                    continue;
                }

                // A machine was created, attach it to the nodes.
                possibleCreated.getNodes().forEach(node -> machineRegistry.put(node, possibleCreated));

                // Also, fire an interact event for the new machine.
                possibleCreated.onNodeInteract(block, event);
                return;
            }
        }

        // No machine existing here, no machine created: Try single block machines.
        final Collection<SingleBlockMachine> singleBlockMachines = singleBlockRegistry.get(block.getType());
        if (singleBlockMachines != null) {
            singleBlockMachines.forEach(singleMachine -> singleMachine.onNodeInteract(block, event));
        }
    }
}
