/*
 * Copyright (c) 2018. All Rights Reserved.
 */

package de.fuchspfoten.fuchslib.multiblock;

import org.bukkit.block.Block;

import java.util.Collection;
import java.util.Collections;

/**
 * A machine which only uses a single block.
 */
public abstract class SingleBlockMachine implements Machine {

    @Override
    public Collection<Block> getNodes() {
        return Collections.emptyList();
    }
}
