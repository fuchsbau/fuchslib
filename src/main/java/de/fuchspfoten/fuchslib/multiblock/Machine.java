/*
 * Copyright (c) 2018. All Rights Reserved.
 */

package de.fuchspfoten.fuchslib.multiblock;

import org.bukkit.block.Block;
import org.bukkit.event.player.PlayerInteractEvent;

import java.util.Collection;

/**
 * A multi-block machine.
 */
public interface Machine {

    /**
     * Returns the nodes of the machine.
     *
     * @return The nodes of the machine.
     */
    Collection<Block> getNodes();

    /**
     * Listener for node interaction.
     *
     * @param node  The node that was interacted with.
     * @param event The interaction event.
     */
    void onNodeInteract(Block node, PlayerInteractEvent event);
}
