/*
 * Copyright (c) 2018. All Rights Reserved.
 */

package de.fuchspfoten.fuchslib.multiblock;

import org.bukkit.block.Block;

/**
 * A machine factory provides a way to recognize multi-block machine structures.
 */
public interface MachineFactory {

    /**
     * Attempts to create a machine at the given block. If no machine can be created, {@code null} is returned.
     *
     * @param block The block where creation is attempted at.
     * @return The machine, if successful.
     */
    Machine tryCreateAt(final Block block);
}
