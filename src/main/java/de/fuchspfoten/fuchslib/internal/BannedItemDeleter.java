/*
 * Copyright (c) 2018. All Rights Reserved.
 */

package de.fuchspfoten.fuchslib.internal;

import org.bukkit.entity.HumanEntity;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityPickupItemEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryOpenEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;

/**
 * A listener that deletes banned items.
 */
public class BannedItemDeleter implements Listener {

    /**
     * Marks banned items.
     */
    public static final String BAN_MARKER = "§b§a§n§n§e§d";

    /**
     * Removes banned items from the given inventory.
     *
     * @param inv The inventory.
     */
    private static void removeBanned(final PlayerInventory inv) {
        // Remove from main inventory.
        for (int i = 0; i < inv.getSize(); i++) {
            if (isBanned(inv.getItem(i))) {
                inv.setItem(i, null);
            }
        }

        // Remove from armor.
        final ItemStack[] armor = inv.getArmorContents();
        boolean armorDirty = false;
        for (int i = 0; i < armor.length; i++) {
            if (isBanned(armor[i])) {
                armor[i] = null;
                armorDirty = true;
            }
        }
        if (armorDirty) {
            inv.setArmorContents(armor);
        }

        // Remove from extra contents.
        final ItemStack[] extra = inv.getExtraContents();
        boolean extraDirty = false;
        for (int i = 0; i < extra.length; i++) {
            if (isBanned(extra[i])) {
                extra[i] = null;
                extraDirty = true;
            }
        }
        if (extraDirty) {
            inv.setExtraContents(extra);
        }
    }

    /**
     * Checks whether or not the item is banned.
     *
     * @param item The item.
     * @return true iff it is banned.
     */
    private static boolean isBanned(final ItemStack item) {
        if (item == null || !item.hasItemMeta() || !item.getItemMeta().hasLore()) {
            return false;
        }
        for (final String line : item.getItemMeta().getLore()) {
            if (line.equals(BAN_MARKER)) {
                return true;
            }
        }
        return false;
    }

    @EventHandler
    public void onInventoryClose(final InventoryCloseEvent event) {
        removeBanned(event.getPlayer().getInventory());
    }

    @EventHandler
    public void onInventoryOpen(final InventoryOpenEvent event) {
        removeBanned(event.getPlayer().getInventory());
    }

    @EventHandler
    public void onEntityPickupItem(final EntityPickupItemEvent event) {
        if (event.getEntity() instanceof HumanEntity) {
            removeBanned(((HumanEntity) event.getEntity()).getInventory());
        }
    }
}
