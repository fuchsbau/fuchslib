/*
 * Copyright (c) 2019. All Rights Reserved.
 */

package de.fuchspfoten.fuchslib.internal;

import de.fuchspfoten.fuchslib.ui.SelectorWheel;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerMoveEvent;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * Manages {@link de.fuchspfoten.fuchslib.ui.SelectorWheel}s.
 */
public class SelectorWheelRegistry implements Listener {

    /**
     * The currently active selector wheels.
     */
    private final Map<UUID, SelectorWheel> activeWheels = new HashMap<>();

    /**
     * Registers a selector wheel for the given target.
     *
     * @param target The target.
     * @param wheel  The selector wheel.
     */
    public void registerSelectorWheel(final UUID target, final SelectorWheel wheel) {
        activeWheels.put(target, wheel);
    }

    /**
     * Unregisters a selector wheel.
     *
     * @param who The target whose active selector wheel is to be unregistered.
     */
    public void unregisterWheel(final UUID who) {
        activeWheels.remove(who);
    }

    @EventHandler
    public void onPlayerMove(final PlayerMoveEvent event) {
        final SelectorWheel wheel = activeWheels.get(event.getPlayer().getUniqueId());
        if (wheel != null) {
            if (event.getTo().getWorld() != wheel.getCenter().getWorld()
                    || event.getTo().distanceSquared(wheel.getCenter()) >= 1) {
                // Hidden due to movement away from the center position.
                wheel.hide(true);
            } else {
                wheel.turn(event.getTo().getDirection().setY(0));
            }
        }
    }

    @EventHandler(priority = EventPriority.LOW)
    public void onPlayerInteract(final PlayerInteractEvent event) {
        if (event.getAction() != Action.LEFT_CLICK_BLOCK && event.getAction() != Action.LEFT_CLICK_AIR) {
            return;
        }

        final SelectorWheel wheel = activeWheels.get(event.getPlayer().getUniqueId());
        if (wheel != null) {
            wheel.select();
            event.setCancelled(true);
        }
    }
}