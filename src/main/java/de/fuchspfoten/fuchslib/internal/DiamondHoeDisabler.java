/*
 * Copyright (c) 2018. All Rights Reserved.
 */

package de.fuchspfoten.fuchslib.internal;

import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.inventory.CraftItemEvent;
import org.bukkit.event.inventory.PrepareItemCraftEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

/**
 * A listener that disables diamond hoes.
 */
public class DiamondHoeDisabler implements Listener {

    @EventHandler
    public void onPrepareItemCraft(final PrepareItemCraftEvent event) {
        if (event.getRecipe() == null) {
            return;
        }

        final ItemStack result = event.getRecipe().getResult();
        if (result != null && result.getType() == Material.DIAMOND_HOE && result.getDurability() == 0) {
            event.getInventory().setResult(null);
        }
    }

    @EventHandler
    public void onCraftItem(final CraftItemEvent event) {
        final ItemStack result = event.getRecipe().getResult();
        if (result != null && result.getType() == Material.DIAMOND_HOE && result.getDurability() == 0) {
            event.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onPlayerInteract(final PlayerInteractEvent event) {
        if (event.getAction() == Action.RIGHT_CLICK_BLOCK) {
            final ItemStack item = event.getItem();
            if (item != null && item.getType() == Material.DIAMOND_HOE) {
                event.setCancelled(true);
            }
        }
    }
}
