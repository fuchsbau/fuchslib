/*
 * Copyright (c) 2019. All Rights Reserved.
 */

package de.fuchspfoten.fuchslib.internal.command;

import de.fuchspfoten.fuchslib.FuchsLibPlugin;
import de.fuchspfoten.fuchslib.Messenger;
import de.fuchspfoten.fuchslib.command.LeafCommand;
import org.bukkit.command.CommandSender;

/**
 * /fuchslib reloadmessages reloads all messages of the {@link de.fuchspfoten.fuchslib.Messenger}.
 */
public class ReloadMessageCommand extends LeafCommand {

    /**
     * Constructor.
     */
    protected ReloadMessageCommand() {
        super("fuchslib.help.reloadMessages", "fuchslib.reloadmessages");
        Messenger.register("fuchslib.command.messagesReloaded");
    }

    @Override
    protected void invoke(final CommandSender sender, final String[] args) {
        FuchsLibPlugin.getSelf().reloadMessages();
        Messenger.send(sender, "fuchslib.command.messagesReloaded");
    }
}
