/*
 * Copyright (c) 2018. All Rights Reserved.
 */

package de.fuchspfoten.fuchslib.internal.command;

import de.fuchspfoten.fuchslib.command.TreeCommand;

/**
 * /fuchslib is available for management of the library.
 */
public class FuchslibCommand extends TreeCommand {

    /**
     * Constructor.
     */
    public FuchslibCommand() {
        super(null);
        addSubCommand("reloadmessages", new ReloadMessageCommand());
    }
}
