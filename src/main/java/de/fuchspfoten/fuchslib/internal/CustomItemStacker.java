/*
 * Copyright (c) 2019. All Rights Reserved.
 */

package de.fuchspfoten.fuchslib.internal;

import de.fuchspfoten.fuchslib.item.CustomItems;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityPickupItemEvent;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryHolder;
import org.bukkit.inventory.ItemStack;

import java.util.Random;

/**
 * Takes care of stacking custom items.
 */
public class CustomItemStacker implements Listener {

    /**
     * Adds an item to the storage of the given inventory. Modifies the stored item.
     *
     * @param inventory    The inventory.
     * @param toStore      The item to store.
     * @param maxStackSize The maximum stack size to accept for the given item to store.
     * @return {@code true} iff something was added.
     */
    private static boolean addToInventory(final Inventory inventory, final ItemStack toStore, final int maxStackSize) {
        final ItemStack[] storage = inventory.getStorageContents();
        final boolean dirty = addToArrayStorage(storage, 0, storage.length, toStore, maxStackSize);
        if (dirty) {
            inventory.setStorageContents(storage);
        }
        return dirty;
    }

    /**
     * Adds an item to the given storage. Modifies the stored item and the storage.
     *
     * @param storage      The storage.
     * @param rangeStart   The range start in the array.
     * @param rangeEnd     The range end in the array (exclusive).
     * @param toStore      The item to store.
     * @param maxStackSize The maximum stack size to accept for the given item to store.
     * @return {@code true} iff something was added.
     */
    private static boolean addToArrayStorage(final ItemStack[] storage, final int rangeStart, final int rangeEnd,
                                             final ItemStack toStore, final int maxStackSize) {
        boolean storedSomething = false;
        // First run: add to existing stacks.
        for (int i = rangeStart; i < rangeEnd; i++) {
            final ItemStack stack = storage[i];
            if (stack == null || !stack.isSimilar(toStore)) {
                continue;
            }

            final int needToSatisfy = toStore.getAmount();
            if (needToSatisfy == 0) {
                break;
            }

            final int satisfiable = maxStackSize - stack.getAmount();
            if (satisfiable > 0) {
                storedSomething = true;
                if (needToSatisfy <= satisfiable) {
                    // Can put all.
                    stack.setAmount(stack.getAmount() + needToSatisfy);
                    toStore.setAmount(0);
                } else {
                    // Can put some.
                    stack.setAmount(maxStackSize);
                    toStore.setAmount(needToSatisfy - satisfiable);
                }
            }
        }

        // Second run: add to empty stacks.
        if (toStore.getAmount() < 1) {
            return storedSomething;
        }
        for (int i = rangeStart; i < rangeEnd; i++) {
            if (storage[i] == null || storage[i].getType() == Material.AIR) {
                storage[i] = toStore.clone();
                toStore.setAmount(0);
                return true;
            }
        }
        return storedSomething;
    }

    /**
     * Fetches the custom item from the given item stack.
     *
     * @param stack The item stack.
     * @return The custom item or null.
     */
    private static CustomItems getCustomItemFromStack(final ItemStack stack) {
        if (stack == null || stack.getType() != Material.DIAMOND_HOE) {
            return null;
        }
        return CustomItems.getByDurability(stack.getDurability());
    }

    /**
     * Random number generator for this class.
     */
    private final Random random = new Random();

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onPlayerPickupItem(final EntityPickupItemEvent event) {
        if (event.isCancelled()) {
            return;
        }

        if (!(event.getEntity() instanceof Player)) {
            return;
        }

        // Ensure we have a custom item.
        final ItemStack item = event.getItem().getItemStack();
        final CustomItems custom = getCustomItemFromStack(item);
        if (custom == null) {
            return;
        }

        // Only handle if max stack size > 1.
        if (custom.getMaxStackSize() <= 1) {
            return;
        }

        // We take over handling the event, so we cancel it.
        event.setCancelled(true);
        final boolean changed = addToInventory(((InventoryHolder) event.getEntity()).getInventory(), item,
                custom.getMaxStackSize());
        if (changed) {
            ((Player) event.getEntity()).playSound(event.getItem().getLocation(), Sound.ENTITY_ITEM_PICKUP,
                    0.2f, ((random.nextFloat() - random.nextFloat()) * 0.7f + 1.0f) * 2.0f);
        }
        if (item.getAmount() == 0) {
            // Despawn if completely picked up.
            event.getItem().remove();
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onInventoryClick(final InventoryClickEvent event) {
        if (event.isCancelled()) {
            return;
        }

        final ItemStack cursorItem = event.getCursor();
        final ItemStack clickedItem = event.getCurrentItem();
        final CustomItems customCursor = getCustomItemFromStack(cursorItem);
        final CustomItems customClicked = getCustomItemFromStack(clickedItem);

        // Combining stacks.
        if (event.getClick() == ClickType.LEFT && customCursor != null && customClicked != null) {
            if (customCursor.getMaxStackSize() > 1) {
                if (clickedItem.isSimilar(cursorItem)) {
                    final int amountSum = clickedItem.getAmount() + cursorItem.getAmount();
                    if (amountSum > customClicked.getMaxStackSize()) {
                        // Can place some.
                        clickedItem.setAmount(customClicked.getMaxStackSize());
                        cursorItem.setAmount(amountSum - customClicked.getMaxStackSize());
                    } else {
                        // Can place all.
                        clickedItem.setAmount(amountSum);
                        event.getView().setCursor(null);
                    }
                    event.setCancelled(true);
                }
            }
        }

        // Shift-moving items.
        if (event.getClick() == ClickType.SHIFT_LEFT && customClicked != null) {
            if (customClicked.getMaxStackSize() > 1) {
                if (event.getView().getTopInventory().getType() == InventoryType.CRAFTING) {
                    // Only player inventory open (and a crafting area).
                    final ItemStack[] storage = event.getView().getBottomInventory().getStorageContents();
                    final boolean dirty;
                    if (event.getSlot() < 9) {
                        // Move from hotbar to upper player inventory.
                        dirty = addToArrayStorage(storage, 9, storage.length, clickedItem,
                                customClicked.getMaxStackSize());
                    } else {
                        // Move from upper inventory to hotbar.
                        dirty = addToArrayStorage(storage, 0, 9, clickedItem,
                                customClicked.getMaxStackSize());
                    }
                    if (dirty) {
                        event.getView().getBottomInventory().setStorageContents(storage);
                        if (clickedItem.getAmount() == 0) {
                            event.setCurrentItem(null);
                        }
                    }
                } else {
                    // Move from one inventory to the other.
                    if (event.getClickedInventory() == event.getView().getTopInventory()) {
                        // Top -> Bottom.
                        addToInventory(event.getView().getBottomInventory(), clickedItem,
                                customClicked.getMaxStackSize());
                    } else {
                        // Bottom -> Top.
                        addToInventory(event.getView().getTopInventory(), clickedItem, customClicked.getMaxStackSize());
                    }
                    if (clickedItem.getAmount() == 0) {
                        event.setCurrentItem(null);
                    }
                }
                event.setCancelled(true);
            }
        }
    }
}
