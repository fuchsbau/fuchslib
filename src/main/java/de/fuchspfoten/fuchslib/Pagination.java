/*
 * Copyright (c) 2018. All Rights Reserved.
 */

package de.fuchspfoten.fuchslib;

import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ClickEvent.Action;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.command.CommandSender;

/**
 * Provides help with paginations.
 */
public final class Pagination {

    /**
     * Registers message formats used by the pagination.
     */
    public static void registerFormats() {
        Messenger.register("pages.start");
        Messenger.register("pages.left");
        Messenger.register("pages.inter");
        Messenger.register("pages.right");
        Messenger.register("pages.end");
    }

    /**
     * Shows a clickable pagination control.
     *
     * @param sender       The command sender.
     * @param page         The page.
     * @param pages        The maximum number of pages.
     * @param actionPrefix The prefix for pagination tools.
     */
    public static void showPagination(final CommandSender sender, final int page, final int pages,
                                      final String actionPrefix) {
        final int pageBefore = (page == 1) ? 1 : page - 1;
        final int pageAfter = (page == pages) ? pages : page + 1;

        final BaseComponent complete = new TextComponent();
        for (final BaseComponent beforeSub : TextComponent.fromLegacyText(Messenger.getFormat("pages.start"))) {
            complete.addExtra(beforeSub);
        }
        for (final BaseComponent leftSub : TextComponent.fromLegacyText(Messenger.getFormat("pages.left"))) {
            leftSub.setClickEvent(new ClickEvent(Action.RUN_COMMAND, actionPrefix + pageBefore));
            complete.addExtra(leftSub);
        }

        final String formatted = String.format(Messenger.getFormat("pages.inter"), page, pages);
        for (final BaseComponent interSub : TextComponent.fromLegacyText(formatted)) {
            complete.addExtra(interSub);
        }

        for (final BaseComponent rightSub : TextComponent.fromLegacyText(Messenger.getFormat("pages.right"))) {
            rightSub.setClickEvent(new ClickEvent(Action.RUN_COMMAND, actionPrefix + pageAfter));
            complete.addExtra(rightSub);
        }
        for (final BaseComponent afterSub : TextComponent.fromLegacyText(Messenger.getFormat("pages.end"))) {
            complete.addExtra(afterSub);
        }

        sender.spigot().sendMessage(complete);
    }

    /**
     * Private constructor to prevent instance creation.
     */
    private Pagination() {
    }
}
