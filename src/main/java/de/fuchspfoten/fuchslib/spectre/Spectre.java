/*
 * Copyright (c) 2019. All Rights Reserved.
 */

package de.fuchspfoten.fuchslib.spectre;

import com.comphenix.packetwrapper.WrapperPlayServerEntityDestroy;
import com.comphenix.packetwrapper.WrapperPlayServerEntityMetadata;
import com.comphenix.packetwrapper.WrapperPlayServerRelEntityMove;
import com.comphenix.protocol.wrappers.WrappedDataWatcher.Registry;
import com.comphenix.protocol.wrappers.WrappedDataWatcher.WrappedDataWatcherObject;
import com.comphenix.protocol.wrappers.WrappedWatchableObject;
import de.fuchspfoten.fuchslib.util.ReflectionHelper;
import lombok.Getter;
import org.bukkit.Location;
import org.bukkit.entity.Player;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Set;
import java.util.WeakHashMap;

/**
 * Base class for spectres.
 */
public abstract class Spectre {

    /**
     * The entity ID of this armor stand.
     */
    protected final int entityId;

    /**
     * The location of the armor stand.
     */
    protected final Location location;

    /**
     * The set of players that are able to view this armor stand.
     */
    protected final Set<Player> viewerSet = Collections.newSetFromMap(new WeakHashMap<>());

    /**
     * The unsafe operations manager.
     */
    private final Unsafe unsafe = new Unsafe();

    /**
     * The name of the armor stand.
     */
    private @Getter String name;

    /**
     * The current status byte.
     */
    private byte status;

    /**
     * Constructor.
     */
    protected Spectre(final Location location, final Collection<Player> viewers) {
        entityId = ReflectionHelper.getEntityId();
        this.location = location;
        viewerSet.addAll(viewers);
    }

    /**
     * Changes the name tag of the entity. {@code null} makes the name invisible.
     *
     * @param name The name.
     */
    public void setNameTag(final String name) {
        if (name == null) {
            unsafe.setNameMeta("", false);
        } else {
            unsafe.setNameMeta(name, true);
        }
        this.name = name;
    }

    /**
     * Changes whether or not this entity is invisible.
     *
     * @param invisible {@code true} iff this entity is invisible.
     */
    public void setInvisible(final boolean invisible) {
        if (invisible) {
            status |= (byte) 0x20;
        } else {
            status &= (byte) ~0x20;
        }
        unsafe.setStatusByte(status);
    }

    /**
     * Retrieves the location.
     *
     * @return The location.
     */
    public Location getLocation() {
        return location.clone();
    }

    /**
     * Moves the spectre by the given delta.
     *
     * @param xDelta The x delta.
     * @param yDelta The y delta.
     * @param zDelta The z delta.
     */
    public void move(final double xDelta, final double yDelta, final double zDelta) {
        final double deltaSquared = xDelta * xDelta + yDelta * yDelta + zDelta * zDelta;
        if (deltaSquared >= 64) {
            // TODO teleport.
        } else {
            // TODO onGround?
            location.add(xDelta, yDelta, zDelta);
            unsafe.relativeMove(xDelta, yDelta, zDelta, true);
        }
    }

    /**
     * Removes the spectre.
     */
    public void remove() {
        unsafe.remove();
    }

    /**
     * Unsafe operations.
     */
    protected class Unsafe {

        /**
         * Changes the name metadata.
         *
         * @param name  The new name of the spectre.
         * @param shown {@code true} iff the name should be shown.
         */
        protected void setNameMeta(final String name, final boolean shown) {
            final WrapperPlayServerEntityMetadata wrapper = new WrapperPlayServerEntityMetadata();
            wrapper.setEntityID(entityId);
            wrapper.setMetadata(Arrays.asList(
                    new WrappedWatchableObject(new WrappedDataWatcherObject(2, Registry.get(String.class)), name),
                    new WrappedWatchableObject(new WrappedDataWatcherObject(3, Registry.get(Boolean.class)),
                            shown)
            ));
            viewerSet.forEach(wrapper::sendPacket);
        }

        /**
         * Sets the entity status byte of this spectre.
         *
         * @param status The entity status byte.
         */
        protected void setStatusByte(final byte status) {
            final WrapperPlayServerEntityMetadata wrapper = new WrapperPlayServerEntityMetadata();
            wrapper.setEntityID(entityId);
            wrapper.setMetadata(Collections.singletonList(
                    new WrappedWatchableObject(new WrappedDataWatcherObject(0, Registry.get(Byte.class)), status)
            ));
            viewerSet.forEach(wrapper::sendPacket);
        }

        /**
         * Removes the spectre.
         */
        protected void remove() {
            final WrapperPlayServerEntityDestroy wrapper = new WrapperPlayServerEntityDestroy();
            wrapper.setEntityIds(new int[]{entityId});
            viewerSet.forEach(wrapper::sendPacket);
        }

        /**
         * Performs a relative move of the entity.
         *
         * @param dx       The x delta.
         * @param dy       The y delta.
         * @param dz       The z delta.
         * @param onGround Whether or not the entity is on ground.
         */
        protected void relativeMove(final double dx, final double dy, final double dz, final boolean onGround) {
            final WrapperPlayServerRelEntityMove wrapper = new WrapperPlayServerRelEntityMove();
            wrapper.setEntityID(entityId);
            wrapper.setDx((int) (dx * 4096));
            wrapper.setDy((int) (dy * 4096));
            wrapper.setDz((int) (dz * 4096));
            wrapper.setOnGround(onGround);
            viewerSet.forEach(wrapper::sendPacket);
        }
    }
}
