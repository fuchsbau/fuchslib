/*
 * Copyright (c) 2019. All Rights Reserved.
 */

package de.fuchspfoten.fuchslib.spectre;

import com.comphenix.packetwrapper.WrapperPlayServerEntityEquipment;
import com.comphenix.packetwrapper.WrapperPlayServerEntityMetadata;
import com.comphenix.packetwrapper.WrapperPlayServerSpawnEntity;
import com.comphenix.packetwrapper.WrapperPlayServerSpawnEntity.ObjectTypes;
import com.comphenix.protocol.wrappers.EnumWrappers.ItemSlot;
import com.comphenix.protocol.wrappers.WrappedDataWatcher.Registry;
import com.comphenix.protocol.wrappers.WrappedDataWatcher.WrappedDataWatcherObject;
import com.comphenix.protocol.wrappers.WrappedWatchableObject;
import lombok.Getter;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.Collection;
import java.util.Collections;
import java.util.UUID;

/**
 * An armor stand that is maintained via packets.
 */
public class ArmorStandSpectre extends Spectre {

    /**
     * The unsafe operations manager.
     */
    private final Unsafe unsafe = new Unsafe();

    /**
     * Whether or not the armor stand has a base plate.
     */
    private @Getter boolean hasBasePlate;

    /**
     * Constructor.
     *
     * @param location The location at which the armor stand is spawned.
     * @param viewers  The players who are able to view the armor stand.
     */
    public ArmorStandSpectre(final Location location, final Collection<Player> viewers) {
        super(location, viewers);
        unsafe.spawnArmorStand();
    }

    /**
     * Changes whether or not the armor stand has a base plate.
     *
     * @param hasBasePlate Whether or not the armor stand has a base plate.
     */
    public void setHasBasePlate(final boolean hasBasePlate) {
        this.hasBasePlate = hasBasePlate;
        unsafe.setBasePlateMeta(hasBasePlate);
    }

    /**
     * Sets the helmet of the armor stand.
     *
     * @param stack The helmet.
     */
    public void setHelmet(final ItemStack stack) {
        unsafe.setHelmet(stack);
    }

    /**
     * Unsafe operations.
     */
    @SuppressWarnings("ClassNameSameAsAncestorName")
    protected class Unsafe extends Spectre.Unsafe {

        /**
         * Spawns an armor stand.
         */
        protected void spawnArmorStand() {
            final WrapperPlayServerSpawnEntity wrapper = new WrapperPlayServerSpawnEntity();
            wrapper.setEntityID(entityId);
            wrapper.setUniqueId(UUID.randomUUID());
            wrapper.setX(location.getX());
            wrapper.setY(location.getY());
            wrapper.setZ(location.getZ());
            wrapper.setOptionalSpeedX(0);
            wrapper.setOptionalSpeedY(0);
            wrapper.setOptionalSpeedZ(0);
            wrapper.setPitch(location.getPitch());
            wrapper.setYaw(location.getYaw());
            wrapper.setType(ObjectTypes.ARMORSTAND);
            wrapper.setObjectData(0);
            viewerSet.forEach(wrapper::sendPacket);
        }

        /**
         * Changes the base plate metadata.
         *
         * @param hasBasePlate Whether or not the armor stand has a base plate.
         */
        protected void setBasePlateMeta(final boolean hasBasePlate) {
            final WrapperPlayServerEntityMetadata wrapper = new WrapperPlayServerEntityMetadata();
            wrapper.setEntityID(entityId);
            wrapper.setMetadata(Collections.singletonList(
                    new WrappedWatchableObject(new WrappedDataWatcherObject(11, Registry.get(Byte.class)),
                            (byte) (hasBasePlate ? 0x0 : 0x8))
            ));
            viewerSet.forEach(wrapper::sendPacket);
        }

        /**
         * Sets the helmet of this armor stand.
         *
         * @param helmet The helmet stack.
         */
        protected void setHelmet(final ItemStack helmet) {
            final WrapperPlayServerEntityEquipment wrapper = new WrapperPlayServerEntityEquipment();
            wrapper.setEntityID(entityId);
            wrapper.setSlot(ItemSlot.HEAD);
            wrapper.setItem(helmet);
            viewerSet.forEach(wrapper::sendPacket);
        }
    }
}
