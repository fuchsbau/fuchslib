/*
 * Copyright (c) 2019. All Rights Reserved.
 */

package de.fuchspfoten.fuchslib.item;

import org.bukkit.entity.Entity;
import org.bukkit.entity.Item;
import org.bukkit.inventory.ItemStack;

/**
 * Tools for manipulating inventories.
 */
public final class InventoryTools {

    /**
     * Takes up to the given amount of items from the given stack.
     *
     * @param from    The stack to take from.
     * @param max     The maximum amount to take.
     * @param remover Is called when the item was completely removed by this action.
     * @return The amount actually taken.
     */
    public static int takeUpTo(final ItemStack from, final int max, final Runnable remover) {
        if (max == 0) {
            return 0;
        }

        if (from.getAmount() <= max) {
            final int taken = from.getAmount();
            remover.run();
            return taken;
        }
        from.setAmount(from.getAmount() - max);
        return max;
    }

    /**
     * Finds items matching the given {@link de.fuchspfoten.fuchslib.item.ItemMatcher}s in the given iterable of
     * entities in the form of dropped items. Also takes the items, if specified.
     *
     * @param entities     The iterable of entities, containing {@link org.bukkit.entity.Item}s.
     * @param amountToTake The amount to take.
     * @param matchers     The matchers to look for.
     * @return The amount found.
     */
    public static int findInDropped(final Iterable<Entity> entities, final int amountToTake,
                                    final ItemMatcher... matchers) {
        int found = 0;
        int needToTake = amountToTake;
        for (final Entity entity : entities) {
            if (entity instanceof Item) {
                final Item item = (Item) entity;
                final ItemStack stack = item.getItemStack();
                if (stack == null) {
                    continue;
                }

                for (final ItemMatcher matcher : matchers) {
                    matcher.reset(stack);
                    if (matcher.isMatch()) {
                        found += stack.getAmount();
                        if (needToTake > 0) {
                            final int newAmount = Math.max(0, stack.getAmount() - needToTake);
                            final int satisfied = Math.min(needToTake, stack.getAmount());
                            needToTake -= satisfied;
                            if (newAmount == 0) {
                                item.remove();
                            } else {
                                stack.setAmount(newAmount);
                            }
                        }
                        break;
                    }
                }
            }
        }
        return found;
    }

    /**
     * Private constructor to prevent instance creation.
     */
    private InventoryTools() {
    }
}
