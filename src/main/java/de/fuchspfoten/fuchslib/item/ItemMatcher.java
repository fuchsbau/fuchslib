/*
 * Copyright (c) 2018. All Rights Reserved.
 */

package de.fuchspfoten.fuchslib.item;

import de.fuchspfoten.fuchslib.exceptions.NotApplicableException;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

/**
 * Matches and creates items with placeholders.
 */
public class ItemMatcher {

    /**
     * The size limit for the data array.
     */
    private static final int SIZE_LIMIT = 64;

    /**
     * Accepts a format into consumers. The plaintext consumer is called for every plaintext character. The
     * interpolation consumer is called for every {INDEX:TYPE} interpolation sequence. The calls to the consumers are
     * in order of the occurence of the respective objects in the format.
     *
     * @param fmt                   The format.
     * @param plaintextConsumer     The plaintext consumer.
     * @param interpolationConsumer The interpolation consumer.
     */
    private static void acceptFormat(final String fmt, final Consumer<Character> plaintextConsumer,
                                     final BiConsumer<Integer, String> interpolationConsumer) {
        int i = 0;
        while (i < fmt.length()) {
            final char cur = fmt.charAt(i);

            if (cur == '\\') {
                // Escaped characters: Consume two characters (\ and the next character) and keep the second.
                i++;
                if (i < fmt.length()) {
                    plaintextConsumer.accept(fmt.charAt(i));
                }
                i++;
                continue;
            }

            if (cur == '{') {
                // Interpolation sequences.
                final int closing = fmt.indexOf('}', i);
                if (closing == -1) {
                    throw new IllegalArgumentException("Invalid format: '" + fmt + '\'');
                }

                // Separate the {INDEX:TYPE} sequence (sequence does not contain the braces).
                final String sequence = fmt.substring(i + 1, closing);
                final int seqSeparator = sequence.indexOf(':');
                if (seqSeparator == -1) {
                    throw new IllegalArgumentException("Invalid format (no sequence separator): " + fmt);
                }

                // Obtain the index.
                final int idx;
                try {
                    idx = Integer.parseInt(sequence.substring(0, seqSeparator));
                } catch (final NumberFormatException ex) {
                    throw new IllegalArgumentException("Invalid format: '" + fmt + '\'', ex);
                }

                // Check that the index is actually valid.
                if (idx < 0) {
                    throw new IllegalArgumentException("Invalid format: '" + fmt + '\'');
                }

                // Obtain the type.
                final String type = sequence.substring(seqSeparator + 1);

                // Move the cursor to the closing brace. In the next iteration, the cursor will be on the character
                //  after this brace.
                i = closing;
                interpolationConsumer.accept(idx, type);
                i++;
                continue;
            }

            // Regular characters.
            plaintextConsumer.accept(cur);
            i++;
        }
    }

    /**
     * The pattern of this matcher.
     */
    private final ItemStack pattern;

    /**
     * The data retrieved from a match.
     */
    private final Object[] data;

    /**
     * The word-terminating characters.
     */
    private final String wordTerminators;

    /**
     * The number of decimals of floating point numbers to include.
     */
    private final int interpolateDecimals;

    /**
     * Whether or not the last reset operation resulted in a match.
     */
    private @Getter boolean match;

    /**
     * Constructor.
     *
     * @param pattern The pattern of this matcher.
     */
    public ItemMatcher(final ItemStack pattern) {
        this(pattern, 3);
    }

    /**
     * Constructor.
     *
     * @param pattern             The pattern of this matcher.
     * @param interpolateDecimals The number of decimals of floating point numbers to include.
     */
    public ItemMatcher(final ItemStack pattern, final int interpolateDecimals) {
        this(pattern, interpolateDecimals, " \t\n\r\f");
    }

    /**
     * Constructor.
     *
     * @param pattern             The pattern of this matcher.
     * @param interpolateDecimals The number of decimals of floating point numbers to include.
     * @param wordTerminators     Word-terminating characters.
     */
    public ItemMatcher(final ItemStack pattern, final int interpolateDecimals, final String wordTerminators) {
        this.pattern = pattern.clone();

        // The storage for the minimum viable data size.
        final int[] minDataSizeStorage = new int[]{0};

        // Obtain the maximum index used plus one as the minimum viable data size.
        final ItemMeta patternMeta = pattern.getItemMeta();
        if (patternMeta.hasDisplayName()) {
            acceptFormat(patternMeta.getDisplayName(), c -> {
                    },
                    (idx, type) -> minDataSizeStorage[0] = Math.max(minDataSizeStorage[0], idx + 1));
        }
        if (patternMeta.hasLore()) {
            for (final String entry : patternMeta.getLore()) {
                acceptFormat(entry, c -> {
                        },
                        (idx, type) -> minDataSizeStorage[0] = Math.max(minDataSizeStorage[0], idx + 1));
            }
        }

        // Create the data object if the size is not too big.
        if (minDataSizeStorage[0] > SIZE_LIMIT) {
            throw new IllegalArgumentException("Formats exceed maximum data size of " + SIZE_LIMIT);
        }
        data = new Object[minDataSizeStorage[0]];

        this.wordTerminators = wordTerminators;
        this.interpolateDecimals = interpolateDecimals;
    }

    /**
     * Fetches match data for the given index as a string.
     *
     * @param idx The index.
     * @return The data.
     */
    public String getString(final int idx) {
        return (String) data[idx];
    }

    /**
     * Fetches match data for the given index as an integer.
     *
     * @param idx The index.
     * @return The data.
     */
    public int getInt(final int idx) {
        return (int) getLong(idx);
    }

    /**
     * Fetches match data for the given index as a long.
     *
     * @param idx The index.
     * @return The data.
     */
    public long getLong(final int idx) {
        return (Long) data[idx];
    }

    /**
     * Fetches match data for the given index as a float.
     *
     * @param idx The index.
     * @return The data.
     */
    public float getFloat(final int idx) {
        return (float) getDouble(idx);
    }

    /**
     * Fetches match data for the given index as a double.
     *
     * @param idx The index.
     * @return The data.
     */
    public double getDouble(final int idx) {
        return (Double) data[idx];
    }

    /**
     * Fetches match data for the given index.
     *
     * @param idx The index.
     * @return The data.
     */
    public Object get(final int idx) {
        return data[idx];
    }

    /**
     * Returns the number of elements in the match data array.
     *
     * @return The number of elements in the match data array.
     */
    public int getDataSize() {
        return data.length;
    }

    /**
     * Interpolates the pattern using the given data.
     *
     * @param data The data.
     * @return The interpolated item.
     */
    public ItemStack interpolate(final Object... data) {
        final ItemStack item = pattern.clone();
        final ItemMeta meta = item.getItemMeta();
        if (meta.hasDisplayName()) {
            meta.setDisplayName(interpolateString(meta.getDisplayName(), data));
        }
        if (meta.hasLore()) {
            final List<String> lore = meta.getLore();
            for (int i = 0; i < lore.size(); i++) {
                lore.set(i, interpolateString(lore.get(i), data));
            }
            meta.setLore(lore);
        }
        item.setItemMeta(meta);
        return item;
    }

    /**
     * Resets this matcher with the given item.
     *
     * @param item The item.
     */
    public void reset(final ItemStack item) {
        match = false;
        Arrays.fill(data, null);

        // Check trivial equality.
        if (item == null || item.getType() != pattern.getType() || item.getDurability() != pattern.getDurability()) {
            return;
        }
        final ItemMeta patternMeta = pattern.getItemMeta();

        // We need equality without respect to display name and lore.
        final ItemStack cmpClone = item.clone();
        final ItemMeta cloneMeta = cmpClone.getItemMeta();
        cloneMeta.setDisplayName(patternMeta.getDisplayName());
        cloneMeta.setLore(patternMeta.getLore());
        cmpClone.setItemMeta(cloneMeta);

        // Check the equality of with artificially equal display name and lore.
        if (!pattern.isSimilar(cmpClone)) {
            return;
        }

        // Now check that display name and lore might match.
        final ItemMeta actualMeta = item.getItemMeta();

        // First the display name.
        if (actualMeta.hasDisplayName() != patternMeta.hasDisplayName()) {
            return;
        }
        if (actualMeta.hasDisplayName()) {
            if (!canExtrapolate(patternMeta.getDisplayName(), actualMeta.getDisplayName())) {
                // No actual instance.
                return;
            }
        }

        // Second the lore.
        if (actualMeta.hasLore() != patternMeta.hasLore()) {
            return;
        }
        if (actualMeta.hasLore()) {
            final List<String> actualLore = actualMeta.getLore();
            final List<String> patternLore = patternMeta.getLore();
            if (actualLore.size() != patternLore.size()) {
                return;
            }

            for (int i = 0; i < actualLore.size(); i++) {
                if (!canExtrapolate(patternLore.get(i), actualLore.get(i))) {
                    // No actual instance.
                    return;
                }
            }
        }

        // Actually a match.
        match = true;
    }

    /**
     * Extrapolates data from the given instance of the format string. If the given instance is not an instance of the
     * format this method returns false, otherwise it returns true.
     *
     * @param fmt      The format.
     * @param instance The potential instance.
     * @return True iff the potential instance is actually an instance.
     */
    private boolean canExtrapolate(final String fmt, final String instance) {
        final CharScanner scanner = new CharScanner(instance);
        try {
            acceptFormat(fmt, scanner::consume, (idx, type) -> {
                if (data[idx] != null) {
                    // Already set!
                    throw new NotApplicableException("Value of match data array already set.");
                }
                switch (type) {
                    case "s":
                        data[idx] = scanner.consumeWord(wordTerminators);
                        break;
                    case "d":
                        data[idx] = scanner.consumeLong();
                        break;
                    case "f":
                        data[idx] = scanner.consumeDouble();
                        break;
                    default:
                        throw new IllegalArgumentException("Invalid format: '" + fmt + "' with type " + type);
                }
            });
        } catch (final NotApplicableException ex) {
            return false;
        }
        return true;
    }

    /**
     * Interpolates a string with the given data.
     *
     * @param fmt  The format string.
     * @param data The data.
     * @return The interpolated string.
     */
    private String interpolateString(final String fmt, final Object... data) {
        final StringBuilder builder = new StringBuilder();
        acceptFormat(fmt, builder::append, (idx, type) -> {
            if (idx >= data.length) {
                throw new IllegalArgumentException("Invalid format: '" + fmt + "' for argument count " + data.length);
            }
            final Object obj = data[idx];
            if (obj != null) {
                if (obj instanceof Double || obj instanceof Float) {
                    final double dbl = ((Number) obj).doubleValue();
                    builder.append(String.format(Locale.ROOT, "%." + interpolateDecimals + 'f', dbl));
                } else {
                    builder.append(data[idx]);
                }
            } else {
                builder.append("null");
            }
        });
        return builder.toString();
    }

    /**
     * Scanner used for extrapolating strings.
     */
    @RequiredArgsConstructor
    private static class CharScanner {

        /**
         * The data this scanner operates on.
         */
        private final String data;

        /**
         * The position inside the data this scanner is at.
         */
        private int pos = 0;

        /**
         * Consumes the given character. If consuming fails, a {@link NotApplicableException} is thrown.
         *
         * @param c The character.
         */
        public void consume(final char c) {
            if (pos >= data.length() || data.charAt(pos) != c) {
                throw new NotApplicableException();
            }
            pos++;
        }

        /**
         * Consumes a double and returns it. If consuming fails, a {@link NotApplicableException} is thrown.
         *
         * @return The double.
         */
        public double consumeDouble() {
            try {
                return Double.parseDouble(consumeWord("0123456789.-", false));
            } catch (final NumberFormatException ex) {
                throw new NotApplicableException(ex);
            }
        }

        /**
         * Consumes a long and returns it. If consuming fails, a {@link NotApplicableException} is thrown.
         *
         * @return The long.
         */
        public long consumeLong() {
            try {
                return Long.parseLong(consumeWord("0123456789-", false));
            } catch (final NumberFormatException ex) {
                throw new NotApplicableException(ex);
            }
        }

        /**
         * Consumes a word and returns it. This always succeeds. Only characters from the given string terminate words.
         *
         * @param terminators Terminating characters.
         * @return The word.
         */
        public String consumeWord(final String terminators) {
            return consumeWord(terminators, true);
        }

        /**
         * Consumes a word and returns it. This always succeeds. Only characters from the given string terminate words
         * or are included, depending on the second character.
         *
         * @param specialChars Characters with a special meaning.
         * @param exclude      Iff true, characters from terminators are blacklisted. Otherwise they're whitelisted.
         * @return The word.
         */
        private String consumeWord(final String specialChars, final boolean exclude) {
            final StringBuilder builder = new StringBuilder();
            while (pos < data.length()) {
                final char c = data.charAt(pos);
                final boolean found = specialChars.indexOf(c) != -1;
                if (found == exclude) {
                    // Leave the character up and terminate the word.
                    break;
                }
                // Accept the character and move to the next one.
                builder.append(c);
                pos++;
            }
            return builder.toString();
        }
    }
}
