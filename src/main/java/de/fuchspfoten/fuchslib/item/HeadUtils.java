/*
 * Copyright (c) 2019. All Rights Reserved.
 */

package de.fuchspfoten.fuchslib.item;

import com.comphenix.protocol.wrappers.WrappedGameProfile;
import com.comphenix.protocol.wrappers.WrappedSignedProperty;
import de.fuchspfoten.fuchslib.util.ReflectionHelper;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.UUID;

/**
 * Utils for managing heads.
 */
public final class HeadUtils {

    /**
     * Retrieves a skull item for the given UUID and texture.
     *
     * @param uuid    The UUID.
     * @param texture The texture.
     * @return The skull item.
     */
    public static ItemStack getFromUUIDAndTexture(final UUID uuid, final String texture) {
        final ItemStack result = new ItemStack(Material.SKULL_ITEM, 1, (short) 3);
        Unsafe.modifyHeadTexture(result, uuid, texture);
        return result;
    }

    /**
     * Private constructor to prevent instance creation.
     */
    private HeadUtils() {
    }

    /**
     * Unsafe operations.
     */
    private static class Unsafe {

        /**
         * Modifies the head texture.
         *
         * @param stack   The stack to modify.
         * @param uuid    The UUID to set.
         * @param texture The texture to set.
         */
        private static void modifyHeadTexture(final ItemStack stack, final UUID uuid, final String texture) {
            final WrappedGameProfile gameProfile = new WrappedGameProfile(uuid, "name");
            gameProfile.getProperties().put("textures",
                    new WrappedSignedProperty("textures", texture, null));

            final ItemMeta meta = stack.getItemMeta();
            ReflectionHelper.setPrivateField(meta.getClass(), "profile", meta, gameProfile.getHandle());
            stack.setItemMeta(meta);
        }
    }
}
