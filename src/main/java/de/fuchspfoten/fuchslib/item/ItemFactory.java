/*
 * Copyright (c) 2018. All Rights Reserved.
 */

package de.fuchspfoten.fuchslib.item;

import de.fuchspfoten.fuchslib.internal.BannedItemDeleter;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.material.MaterialData;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * A class that can be used for editing items.
 */
public class ItemFactory {

    /**
     * The item stack that is created.
     */
    private final ItemStack item;

    /**
     * Whether or not the item is banned.
     */
    private boolean banned = false;

    /**
     * Constructor.
     */
    public ItemFactory() {
        this(new ItemStack(Material.STONE, 1));
    }

    /**
     * Constructor.
     *
     * @param item The item to base the factory on.
     */
    public ItemFactory(final ItemStack item) {
        this.item = item;
        if (item.hasItemMeta() && item.getItemMeta().hasLore()) {
            if (item.getItemMeta().getLore().contains(BannedItemDeleter.BAN_MARKER)) {
                banned = true;
            }
        }
    }

    /**
     * Sets the type of the item.
     *
     * @param mat The type.
     * @return this, for chaining.
     */
    public ItemFactory type(final Material mat) {
        item.setType(mat);
        return this;
    }

    /**
     * Sets the data of the item.
     *
     * @param data The data.
     * @return this, for chaining.
     */
    public ItemFactory data(final MaterialData data) {
        item.setData(data);
        return this;
    }

    /**
     * Sets the amount of the item.
     *
     * @param amount The amount.
     * @return this, for chaining.
     */
    public ItemFactory amount(final int amount) {
        item.setAmount(amount);
        return this;
    }

    /**
     * Sets the durability of the item.
     *
     * @param dura The durability.
     * @return this, for chaining.
     */
    public ItemFactory durability(final short dura) {
        item.setDurability(dura);
        return this;
    }

    /**
     * Sets the name of the item.
     *
     * @param name The name.
     * @return this, for chaining.
     */
    public ItemFactory name(final String name) {
        final ItemMeta itemMeta = item.getItemMeta();
        itemMeta.setDisplayName(name);
        item.setItemMeta(itemMeta);
        return this;
    }

    /**
     * Enchants this item. Level 0 removes enchantments.
     *
     * @param ench  The enchantment.
     * @param level The level.
     * @return this, for chaining.
     */
    public ItemFactory enchant(final Enchantment ench, final int level) {
        if (level < 1 && item.getEnchantmentLevel(ench) > 0) {
            item.removeEnchantment(ench);
        } else if (level > 0) {
            item.addUnsafeEnchantment(ench, level);
        }
        return this;
    }

    /**
     * Adds lines to the lore of this item.
     *
     * @param lines The lines.
     * @return this, for chaining.
     */
    public ItemFactory lore(final String... lines) {
        final ItemMeta itemMeta = item.getItemMeta();
        final List<String> lore = itemMeta.hasLore() ? itemMeta.getLore() : new ArrayList<>();
        if (banned) {
            // Size > 0.
            for (final String line : lines) {
                lore.add(lore.size() - 1, line);
            }
        } else {
            lore.addAll(Arrays.asList(lines));
        }
        itemMeta.setLore(lore);
        item.setItemMeta(itemMeta);
        return this;
    }

    /**
     * Removes the last line of the lore.
     *
     * @return this, for chaining.
     */
    public ItemFactory popLore() {
        final ItemMeta itemMeta = item.getItemMeta();
        final List<String> lore = itemMeta.hasLore() ? itemMeta.getLore() : new ArrayList<>();
        if (banned && lore.size() > 1) {
            // Delete (n-2)th line. (n-1)th is ban marker.
            lore.remove(lore.size() - 2);
        } else if (!banned && !lore.isEmpty()) {
            // Delete (n-1)th line.
            lore.remove(lore.size() - 1);
        }
        itemMeta.setLore(lore);
        item.setItemMeta(itemMeta);
        return this;
    }

    /**
     * Removes the last line of the lore.
     *
     * @param repeat The number of lines to remove.
     * @return this, for chaining.
     */
    public ItemFactory popLore(final int repeat) {
        for (int i = 0; i < repeat; i++) {
            popLore();
        }
        return this;
    }

    /**
     * Bans this item, making it unobtainable.
     *
     * @return this, for chaining.
     */
    public ItemFactory ban() {
        banned = true;
        final ItemMeta itemMeta = item.getItemMeta();
        final List<String> lore = itemMeta.hasLore() ? itemMeta.getLore() : new ArrayList<>();
        lore.add(BannedItemDeleter.BAN_MARKER);
        itemMeta.setLore(lore);
        item.setItemMeta(itemMeta);
        return this;
    }

    /**
     * Unbans this item, making it obtainable again.
     *
     * @return this, for chaining.
     */
    public ItemFactory unban() {
        if (banned) {
            final ItemMeta itemMeta = item.getItemMeta();
            final List<String> lore = itemMeta.getLore();
            lore.removeIf(x -> x.equals(BannedItemDeleter.BAN_MARKER));
            itemMeta.setLore(lore);
            item.setItemMeta(itemMeta);
        }
        banned = false;
        return this;
    }

    /**
     * Makes the item unbreakable.
     *
     * @return this, for chaining.
     */
    public ItemFactory unbreakable() {
        final ItemMeta itemMeta = item.getItemMeta();
        itemMeta.setUnbreakable(true);
        item.setItemMeta(itemMeta);
        return this;
    }

    /**
     * Hides enchantments on the item.
     *
     * @return this, for chaining.
     */
    public ItemFactory hideEnchants() {
        final ItemMeta itemMeta = item.getItemMeta();
        itemMeta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
        item.setItemMeta(itemMeta);
        return this;
    }

    /**
     * Hides attributes on the item.
     *
     * @return this, for chaining.
     */
    public ItemFactory hideAttributes() {
        final ItemMeta itemMeta = item.getItemMeta();
        itemMeta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
        item.setItemMeta(itemMeta);
        return this;
    }

    /**
     * Hides unbreakable status on the item.
     *
     * @return this, for chaining.
     */
    public ItemFactory hideUnbreakable() {
        final ItemMeta itemMeta = item.getItemMeta();
        itemMeta.addItemFlags(ItemFlag.HIDE_UNBREAKABLE);
        item.setItemMeta(itemMeta);
        return this;
    }

    /**
     * Hides destroyable targets on the item.
     *
     * @return this, for chaining.
     */
    public ItemFactory hideDestroys() {
        final ItemMeta itemMeta = item.getItemMeta();
        itemMeta.addItemFlags(ItemFlag.HIDE_DESTROYS);
        item.setItemMeta(itemMeta);
        return this;
    }

    /**
     * Hides placeable locations on the item.
     *
     * @return this, for chaining.
     */
    public ItemFactory hidePlacedOn() {
        final ItemMeta itemMeta = item.getItemMeta();
        itemMeta.addItemFlags(ItemFlag.HIDE_PLACED_ON);
        item.setItemMeta(itemMeta);
        return this;
    }

    /**
     * Hides potion effects on the item.
     *
     * @return this, for chaining.
     */
    public ItemFactory hidePotionEffects() {
        final ItemMeta itemMeta = item.getItemMeta();
        itemMeta.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
        item.setItemMeta(itemMeta);
        return this;
    }

    /**
     * Hides everything that can be hidden on the item.
     *
     * @return this, for chaining.
     */
    public ItemFactory hideEverything() {
        return hideEnchants().hideAttributes().hideUnbreakable().hideDestroys().hidePlacedOn().hidePotionEffects();
    }

    /**
     * Creates an instance using the item factory.
     *
     * @return The instance.
     */
    public ItemStack instance() {
        return item.clone();
    }

    /**
     * Checks whether the given item is one of the items of this factory.
     *
     * @param item The item.
     * @return Whether it is an item of this factory.
     */
    public boolean is(final ItemStack item) {
        return this.item.isSimilar(item);
    }
}
