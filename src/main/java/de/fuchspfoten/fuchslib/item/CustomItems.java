/*
 * Copyright (c) 2019. All Rights Reserved.
 */

package de.fuchspfoten.fuchslib.item;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.bukkit.Material;

/**
 * Manages custom items.
 */
@RequiredArgsConstructor
public enum CustomItems {

    /**
     * An information symbol.
     */
    INFORMATION_SYMBOL(1, 1),

    /**
     * An arrow pointing to the left.
     */
    ARROW_LEFT(2, 1),

    /**
     * An arrow pointing to the right.
     */
    ARROW_RIGHT(3, 1),

    /**
     * A magnifying glass.
     */
    MAGNIFYING_GLASS(4, 1),

    /**
     * A backpack.
     */
    BACKPACK(5, 1),

    /**
     * A backpack with a golden latch.
     */
    BACKPACK_GOLDEN(6, 1),

    /**
     * A lock with a plus symbol.
     */
    LOCK_PLUS(7, 1),

    /**
     * Oak-tree bark (light color).
     */
    BARK_OAK(8, 64),

    /**
     * Spruce tree bark (dark color).
     */
    BARK_SPRUCE(9, 64),

    /**
     * A sewing kit, a needle and yarn.
     */
    SEWING_KIT(10, 1),

    /**
     * Tanned leather.
     */
    TANNED_LEATHER(11, 64),

    /**
     * A wooden spoon, e.g. for cooking.
     */
    WOODEN_SPOON(12, 1),

    /**
     * A blacksmith's hammer, made with diamonds.
     */
    BLACKSMITH_HAMMER(13, 1),

    /**
     * An editing wand for game administration.
     */
    EDITING_WAND(14, 1);

    /**
     * A by-durability lookup table for custom items.
     */
    private static final CustomItems[] byDurability = new CustomItems[2048];

    static {
        for (final CustomItems custom : values()) {
            byDurability[custom.durability] = custom;
        }
    }

    public static CustomItems getByDurability(final int durability) {
        return byDurability[durability];
    }

    /**
     * The durability that needs to be set for the item.
     */
    private final int durability;

    /**
     * The max stack size for the custom item.
     */
    private @Getter final int maxStackSize;

    /**
     * Retrieves an item factory for the given custom item.
     *
     * @return The item factory.
     */
    public ItemFactory getFactory() {
        return new ItemFactory()
                .type(Material.DIAMOND_HOE)
                .durability((short) durability)
                .unbreakable()
                .hideUnbreakable()
                .hideAttributes();
    }
}
