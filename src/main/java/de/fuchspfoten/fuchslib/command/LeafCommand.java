/*
 * Copyright (c) 2018. All Rights Reserved.
 */

package de.fuchspfoten.fuchslib.command;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

/**
 * Represents a leaf node in a command tree.
 */
@SuppressWarnings("AbstractClassExtendsConcreteClass")
public abstract class LeafCommand extends TreeCommand {

    /**
     * The permission that is required for this command node.
     */
    private final String permission;

    /**
     * Constructor.
     *
     * @param helpLineFormat The help line format for the messenger API.
     */
    protected LeafCommand(final String helpLineFormat, final String permission) {
        super(helpLineFormat);
        this.permission = permission;
    }

    /**
     * Invokes this command node.
     *
     * @param sender The command sender.
     * @param args   The command arguments.
     */
    protected abstract void invoke(final CommandSender sender, final String[] args);

    @Override
    public final boolean onCommand(final CommandSender sender, final Command command, final String label,
                                   final String[] args) {
        if (permission != null) {
            if (!sender.hasPermission(permission)) {
                sender.sendMessage("Missing permission: " + permission);
                return true;
            }
        }

        // Invoke the command.
        invoke(sender, args);

        return true;
    }
}
