/*
 * Copyright (c) 2018. All Rights Reserved.
 */

package de.fuchspfoten.fuchslib.command;

import de.fuchspfoten.fuchslib.Messenger;
import lombok.Getter;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import java.util.Arrays;
import java.util.Map;
import java.util.TreeMap;

/**
 * Represents a node in a command tree.
 */
public class TreeCommand implements CommandExecutor {

    /**
     * The subcommands of this tree command.
     */
    private final Map<String, TreeCommand> subCommands = new TreeMap<>();

    /**
     * The messenger format for the help line.
     */
    private @Getter final String helpLineFormat;

    /**
     * Constructor.
     *
     * @param helpLineFormat The help line format for the messenger API.
     */
    public TreeCommand(final String helpLineFormat) {
        this.helpLineFormat = helpLineFormat;
        if (helpLineFormat != null) {
            Messenger.register(helpLineFormat);
        }
    }

    /**
     * Adds a subcommand.
     *
     * @param key     The key of the subcommand.
     * @param command The command which is stored for this key.
     */
    protected void addSubCommand(final String key, final TreeCommand command) {
        subCommands.put(key, command);
    }

    @Override
    public boolean onCommand(final CommandSender sender, final Command command, final String label,
                             final String[] args) {
        // No arguments: Help.
        if (args.length == 0) {
            for (final TreeCommand tc : subCommands.values()) {
                if (tc.getHelpLineFormat() != null) {
                    Messenger.send(sender, tc.getHelpLineFormat());
                }
            }
            return true;
        }

        // Fetch the subcommand.
        final String subCommand = args[0];
        final TreeCommand treeCommand = subCommands.get(subCommand);

        // Execute the subcommand if possible.
        if (treeCommand != null) {
            treeCommand.onCommand(sender, command, label, Arrays.copyOfRange(args, 1, args.length));
        } else {
            Messenger.send(sender, "treeCommandNotFound", subCommand);
        }

        return true;
    }
}
