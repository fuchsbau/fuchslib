package de.fuchspfoten.fuchslib.internal.tests;

import de.fuchspfoten.fuchslib.FuchsLibPlugin;
import de.fuchspfoten.fuchslib.item.ItemMatcher;
import de.fuchspfoten.fuchslib.tests.BasePluginTest;
import de.fuchspfoten.fuchslib.tests.TestHelper;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;

public class ItemMatcherTest extends BasePluginTest<FuchsLibPlugin> {

    /**
     * Constructor.
     */
    public ItemMatcherTest() {
        super(FuchsLibPlugin.class);
    }

    @Test
    public void testSimpleNoArgsMatch() {
        final ItemMatcher matcher = new ItemMatcher(new ItemStack(Material.DIAMOND));
        final ItemStack item1 = new ItemStack(Material.DIAMOND);
        final ItemStack item2 = new ItemStack(Material.DIRT);
        final ItemStack item3 = matcher.interpolate();
        final ItemStack item4 = matcher.interpolate("abc", 123);

        Assert.assertEquals(0, matcher.getDataSize());

        matcher.reset(item1);
        Assert.assertTrue(matcher.isMatch());
        matcher.reset(item2);
        Assert.assertFalse(matcher.isMatch());
        matcher.reset(item3);
        Assert.assertTrue(matcher.isMatch());
        matcher.reset(item4);
        Assert.assertTrue(matcher.isMatch());
    }

    @Test
    public void testComplexNoArgsMatch() {
        final ItemStack item = new ItemStack(Material.COMPASS);
        final ItemMeta meta = item.getItemMeta();
        meta.setDisplayName("§6Color Name");
        meta.setLore(Arrays.asList("line1", "line2", "line3"));
        item.setItemMeta(meta);

        final ItemMatcher matcher = new ItemMatcher(item);
        final ItemStack item1 = new ItemStack(Material.COMPASS);
        final ItemStack item2 = new ItemStack(Material.DIRT);
        final ItemStack item3 = matcher.interpolate();
        final ItemStack item4 = matcher.interpolate("abc", 123);

        Assert.assertTrue(item3.getItemMeta().hasDisplayName());
        Assert.assertTrue(item4.getItemMeta().hasDisplayName());
        Assert.assertTrue(item3.getItemMeta().hasLore());
        Assert.assertTrue(item4.getItemMeta().hasLore());
        Assert.assertEquals(0, matcher.getDataSize());

        matcher.reset(item1);
        Assert.assertFalse(matcher.isMatch());
        matcher.reset(item2);
        Assert.assertFalse(matcher.isMatch());
        matcher.reset(item3);
        Assert.assertTrue(matcher.isMatch());
        matcher.reset(item4);
        Assert.assertTrue(matcher.isMatch());
    }

    @Test
    public void testMatch() {
        final ItemStack item = new ItemStack(Material.COMPASS);
        final ItemMeta meta = item.getItemMeta();
        meta.setDisplayName("§6ABC{0:s}");
        meta.setLore(Arrays.asList("{1:d}", "{2:f}x", "abc {3:s} xyz"));
        item.setItemMeta(meta);

        final ItemMatcher matcher = new ItemMatcher(item);
        final ItemStack item1 = new ItemStack(Material.COMPASS);
        TestHelper.assertThrows(matcher::interpolate, IllegalArgumentException.class);
        TestHelper.assertThrows(() -> matcher.interpolate(1), IllegalArgumentException.class);
        TestHelper.assertThrows(() -> matcher.interpolate(1, 2), IllegalArgumentException.class);
        TestHelper.assertThrows(() -> matcher.interpolate(1, 2, 3), IllegalArgumentException.class);
        final ItemStack item2 = matcher.interpolate("abc", 123, 2.3781234f, "word");
        final ItemStack item3 = matcher.interpolate("abc", 123, "", "word");

        matcher.reset(item1);
        Assert.assertFalse(matcher.isMatch());

        matcher.reset(item2);
        Assert.assertTrue(matcher.isMatch());
        Assert.assertEquals("abc", matcher.getString(0));
        Assert.assertEquals(123, matcher.getLong(1));
        Assert.assertEquals(2.378f, matcher.getDouble(2), 0.000001f);
        Assert.assertEquals("word", matcher.getString(3));

        matcher.reset(item3);
        Assert.assertFalse(matcher.isMatch());
    }
}
