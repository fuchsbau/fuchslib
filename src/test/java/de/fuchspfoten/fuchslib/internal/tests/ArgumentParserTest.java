/*
 * Copyright (c) 2018. All Rights Reserved.
 */

package de.fuchspfoten.fuchslib.internal.tests;

import de.fuchspfoten.fuchslib.ArgumentParser;
import org.junit.Assert;
import org.junit.Test;

public class ArgumentParserTest {

    @Test
    public void testNoArguments() {
        final ArgumentParser ap = new ArgumentParser();
        final String[] args = new String[]{"Hello", "World!"};
        Assert.assertArrayEquals(args, ap.parse(args));
        Assert.assertFalse(ap.hasArgument('p'));
    }

    @Test
    public void testNoArgumentsButProvided() {
        final ArgumentParser ap = new ArgumentParser();
        final String[] args = new String[]{"-p", "help", "Hello", "World!"};
        final String[] exp = new String[]{"help", "Hello", "World!"};
        Assert.assertArrayEquals(exp, ap.parse(args));
        Assert.assertFalse(ap.hasArgument('p'));
    }

    @Test
    public void testNoArgumentsAfterBlock() {
        final ArgumentParser ap = new ArgumentParser();
        final String[] args = new String[]{"--", "-p", "help", "Hello", "World!"};
        final String[] exp = new String[]{"-p", "help", "Hello", "World!"};
        Assert.assertArrayEquals(exp, ap.parse(args));
        Assert.assertFalse(ap.hasArgument('p'));
    }

    @Test
    public void testArgsNotProvided() {
        final ArgumentParser ap = new ArgumentParser();
        ap.addSwitch('x', "Lorem Ipsum");
        ap.addArgument('z', "Dolor Sit", "Amet");

        final String[] args = new String[]{"Hello", "World!"};
        Assert.assertArrayEquals(args, ap.parse(args));
        Assert.assertFalse(ap.hasArgument('x'));
        Assert.assertFalse(ap.hasArgument('y'));
        Assert.assertFalse(ap.hasArgument('z'));
    }

    @Test
    public void testSwitch() {
        final ArgumentParser ap = new ArgumentParser();
        ap.addSwitch('x', "Lorem Ipsum");

        final String[] args = new String[]{"-x", "hello"};
        final String[] exp = new String[]{"hello"};

        Assert.assertArrayEquals(exp, ap.parse(args));
        Assert.assertTrue(ap.hasArgument('x'));

        Assert.assertArrayEquals(exp, ap.parse(exp));
        Assert.assertFalse(ap.hasArgument('x'));
    }

    @Test
    public void testParam() {
        final ArgumentParser ap = new ArgumentParser();
        ap.addArgument('x', "Lorem Ipsum", "Dolor");

        final String[] args = new String[]{"-x", "hello", "world"};
        final String[] exp = new String[]{"world"};

        Assert.assertArrayEquals(exp, ap.parse(args));
        Assert.assertEquals("hello", ap.getArgument('x'));

        Assert.assertArrayEquals(exp, ap.parse(exp));
        Assert.assertNull(ap.getArgument('x'));
    }

    @Test
    public void testMultiSwitch() {
        final ArgumentParser ap = new ArgumentParser();
        ap.addSwitch('x', "Lorem Ipsum");
        ap.addSwitch('y', "Dolor Sit");

        final String[] args = new String[]{"-x", "-y", "hello"};
        final String[] exp = new String[]{"hello"};

        Assert.assertArrayEquals(exp, ap.parse(args));
        Assert.assertTrue(ap.hasArgument('x'));
        Assert.assertTrue(ap.hasArgument('y'));

        Assert.assertArrayEquals(exp, ap.parse(exp));
        Assert.assertFalse(ap.hasArgument('x'));
        Assert.assertFalse(ap.hasArgument('y'));

        final String[] args2 = new String[]{"-xy", "hello"};

        Assert.assertArrayEquals(exp, ap.parse(args2));
        Assert.assertTrue(ap.hasArgument('x'));
        Assert.assertTrue(ap.hasArgument('y'));
    }

    @Test
    public void testMultiSwitchParam() {
        final ArgumentParser ap = new ArgumentParser();
        ap.addSwitch('x', "Lorem Ipsum");
        ap.addSwitch('y', "Dolor Sit");
        ap.addArgument('z', "Amet", "Consectetur");

        final String[] args = new String[]{"-yxz", "hello"};
        final String[] exp = new String[]{};
        final String[] exp2 = new String[]{"hello"};

        Assert.assertArrayEquals(exp, ap.parse(args));
        Assert.assertTrue(ap.hasArgument('x'));
        Assert.assertTrue(ap.hasArgument('y'));
        Assert.assertEquals("hello", ap.getArgument('z'));

        Assert.assertArrayEquals(exp, ap.parse(exp));
        Assert.assertFalse(ap.hasArgument('x'));
        Assert.assertFalse(ap.hasArgument('y'));
        Assert.assertFalse(ap.hasArgument('z'));

        final String[] args2 = new String[]{"-xzy", "hello"};

        Assert.assertArrayEquals(exp2, ap.parse(args2));
        Assert.assertTrue(ap.hasArgument('x'));
        Assert.assertTrue(ap.hasArgument('y'));
        Assert.assertEquals("", ap.getArgument('z'));
    }
}
